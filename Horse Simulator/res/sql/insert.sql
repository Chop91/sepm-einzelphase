BEGIN;
DELETE FROM RaceSimulation;
DELETE FROM Jockey;
DELETE FROM Horse;
COMMIT;

INSERT INTO Horse(Name, Age, MinSpeed, MaxSpeed, ImagePath) VALUES
	('Rainbow Dash', 5, 50.0, 60.0, 'res/images/rainbow-dash.png'),
	('Twilight Sparkle', 4, 40.0, 50.0, 'res/images/twilight-sparkle.png'),
	('Applejack', 6, 45.0, 58.0, 'res/images/applejack.png'),
	('Fluttershy', 5, 40.0, 45.0, 'res/images/fluttershy.png'),
	('Pinkie Pie', 4, 45.0, 49.0, 'res/images/pinkie-pie.png'),
	('Rarity', 5, 42.0, 46.0, 'res/images/rarity.png'),
	('Derpy', 4, 40.0, 60.0, 'res/images/derpy.png'),
	('Cadance', 8, 44.0, 52.0, 'res/images/cadance.png'),
	('Luna', 10, 45.0, 60.0, 'res/images/luna.png'),
	('Celestia', 10, 45.0, 58.0, 'res/images/celestia.png');

INSERT INTO Jockey(FirstName, LastName, Age, Skill, ImagePath) VALUES
	('Donald', 'Trump', 18, -9.0, 'res/images/trump.png'),
	('Hillary', 'Clinton', 20, 2.0, 'res/images/clinton.png'),
	('Heinz-Christian', 'Strache', 27, -7.0, 'res/images/strache.png'),
	('Norbert', 'Hofer', 25, -3.0, 'res/images/hofer.png'),
	('Werner', 'Faymann', 24, 2.0, 'res/images/faymann.png'),
	('Rudolf', 'Hundstorfer', 23, 4.0, 'res/images/hundstorfer.png'),
	('Alexander', 'Van der Bellen', 23, 6.0, 'res/images/van-der-bellen.png'),
	('Richard', 'Lugner', 22, 4.0, 'res/images/lugner.png'),
	('Andreas', 'Khol', 21, 3.0, 'res/images/khol.png'),
	('Irmgard', 'Griss', 26, -1.0, 'res/images/griss.png');

INSERT INTO RaceSimulation(Id, HorseId, HorseName, JockeyId, JockeyName, AverageSpeed, RandomSpeed, LuckFactor, CalculatedSkill, Rank) VALUES
	(RaceSimulationSequence.NEXTVAL, 1, 'Rainbow Dash',  1, 'Donald Trump', 60.0, 50.0, 1.0, 1.0, 1),
	(RaceSimulationSequence.CURRVAL, 2, 'Twilight Sparkle', 2, 'Hillary Clinton', 59.0, 50.0, 1.0, 1.0, 2),
	(RaceSimulationSequence.CURRVAL, 3, 'Applejack', 3, 'Heinz-Christian Strache', 58.0, 50.0, 1.0, 1.0, 3),
	(RaceSimulationSequence.CURRVAL, 4, 'Fluttershy', 4, 'Norbert Hofer', 57.0, 50.0, 1.0, 1.0, 4),
	(RaceSimulationSequence.CURRVAL, 5, 'Pinkie Pie', 5, 'Werner Faymann', 56.0, 50.0, 1.0, 1.0, 5),
	(RaceSimulationSequence.CURRVAL, 6, 'Rarity', 6, 'Rudolf Hundstorfer', 55.0, 50.0, 1.0, 1.0, 6),
	(RaceSimulationSequence.CURRVAL, 7, 'Derpy', 7, 'Alexander Van der Bellen', 54.0, 50.0, 1.0, 1.0, 7),
	(RaceSimulationSequence.CURRVAL, 8, 'Cadance', 8, 'Richard Lugner', 53.0, 50.0, 1.0, 1.0, 8),
	(RaceSimulationSequence.CURRVAL, 9, 'Luna', 9, 'Andreas Khol', 52.0, 50.0, 1.0, 1.0, 9),
	(RaceSimulationSequence.CURRVAL, 10, 'Celestia', 10, 'Irmgard Griss', 51.0, 50.0, 1.0, 1.0, 10),
	(RaceSimulationSequence.NEXTVAL, 10, 'Rainbow Dash',  1, 'Donald Trump', 60.0, 50.0, 1.0, 1.0, 10),
	(RaceSimulationSequence.CURRVAL, 9, 'Twilight Sparkle', 2, 'Hillary Clinton', 59.0, 50.0, 1.0, 1.0, 9),
	(RaceSimulationSequence.CURRVAL, 8, 'Applejack', 3, 'Heinz-Christian Strache', 58.0, 50.0, 1.0, 1.0, 8),
	(RaceSimulationSequence.CURRVAL, 7, 'Fluttershy', 4, 'Norbert Hofer', 57.0, 50.0, 1.0, 1.0, 7),
	(RaceSimulationSequence.CURRVAL, 6, 'Pinkie Pie', 5, 'Werner Faymann', 56.0, 50.0, 1.0, 1.0, 6),
	(RaceSimulationSequence.CURRVAL, 5, 'Rarity', 6, 'Rudolf Hundstorfer', 55.0, 50.0, 1.0, 1.0, 5),
	(RaceSimulationSequence.CURRVAL, 4, 'Derpy', 7, 'Alexander Van der Bellen', 54.0, 50.0, 1.0, 1.0, 4),
	(RaceSimulationSequence.CURRVAL, 3, 'Cadance', 8, 'Richard Lugner', 53.0, 50.0, 1.0, 1.0, 3),
	(RaceSimulationSequence.CURRVAL, 2, 'Luna', 9, 'Andreas Khol', 52.0, 50.0, 1.0, 1.0, 2),
	(RaceSimulationSequence.CURRVAL, 1, 'Celestia', 10, 'Irmgard Griss', 51.0, 50.0, 1.0, 1.0, 1),
	(RaceSimulationSequence.NEXTVAL, 1, 'Rainbow Dash',  1, 'Donald Trump', 60.0, 50.0, 1.0, 1.0, 1),
	(RaceSimulationSequence.NEXTVAL, 1, 'Rainbow Dash',  1, 'Donald Trump', 60.0, 50.0, 1.0, 1.0, 2),
	(RaceSimulationSequence.NEXTVAL, 1, 'Rainbow Dash',  1, 'Donald Trump', 60.0, 50.0, 1.0, 1.0, 3),
	(RaceSimulationSequence.NEXTVAL, 1, 'Rainbow Dash',  1, 'Donald Trump', 60.0, 50.0, 1.0, 1.0, 1),
	(RaceSimulationSequence.NEXTVAL, 1, 'Rainbow Dash',  1, 'Donald Trump', 60.0, 50.0, 1.0, 1.0, 2),
	(RaceSimulationSequence.NEXTVAL, 1, 'Rainbow Dash',  1, 'Donald Trump', 60.0, 50.0, 1.0, 1.0, 3),
	(RaceSimulationSequence.NEXTVAL, 1, 'Rainbow Dash',  1, 'Donald Trump', 60.0, 50.0, 1.0, 1.0, 1),
	(RaceSimulationSequence.NEXTVAL, 1, 'Rainbow Dash',  1, 'Donald Trump', 60.0, 50.0, 1.0, 1.0, 2),
	(RaceSimulationSequence.NEXTVAL, 1, 'Rainbow Dash',  1, 'Donald Trump', 60.0, 50.0, 1.0, 1.0, 3),
	(RaceSimulationSequence.NEXTVAL, 1, 'Rainbow Dash',  1, 'Donald Trump', 60.0, 50.0, 1.0, 1.0, 1),
	(RaceSimulationSequence.NEXTVAL, 1, 'Rainbow Dash',  1, 'Donald Trump', 60.0, 50.0, 1.0, 1.0, 2),
	(RaceSimulationSequence.NEXTVAL, 1, 'Rainbow Dash',  1, 'Donald Trump', 60.0, 50.0, 1.0, 1.0, 3),
	(RaceSimulationSequence.NEXTVAL, 1, 'Rainbow Dash',  1, 'Donald Trump', 60.0, 50.0, 1.0, 1.0, 4),
	(RaceSimulationSequence.NEXTVAL, 1, 'Rainbow Dash',  1, 'Donald Trump', 60.0, 50.0, 1.0, 1.0, 5),
	(RaceSimulationSequence.NEXTVAL, 1, 'Rainbow Dash',  1, 'Donald Trump', 60.0, 50.0, 1.0, 1.0, 6),
	(RaceSimulationSequence.NEXTVAL, 1, 'Rainbow Dash',  1, 'Donald Trump', 60.0, 50.0, 1.0, 1.0, 7),
	(RaceSimulationSequence.NEXTVAL, 1, 'Rainbow Dash',  1, 'Donald Trump', 60.0, 50.0, 1.0, 1.0, 8),
	(RaceSimulationSequence.NEXTVAL, 1, 'Rainbow Dash',  1, 'Donald Trump', 60.0, 50.0, 1.0, 1.0, 9),
	(RaceSimulationSequence.NEXTVAL, 1, 'Rainbow Dash',  1, 'Donald Trump', 60.0, 50.0, 1.0, 1.0, 10),
	(RaceSimulationSequence.NEXTVAL, 1, 'Rainbow Dash',  1, 'Donald Trump', 60.0, 50.0, 1.0, 1.0, 5),
	(RaceSimulationSequence.NEXTVAL, 1, 'Rainbow Dash',  1, 'Donald Trump', 60.0, 50.0, 1.0, 1.0, 8),
	(RaceSimulationSequence.NEXTVAL, 1, 'Rainbow Dash',  1, 'Donald Trump', 60.0, 50.0, 1.0, 1.0, 8);
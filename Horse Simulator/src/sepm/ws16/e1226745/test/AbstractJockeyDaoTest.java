package sepm.ws16.e1226745.test;

import org.junit.Test;
import sepm.ws16.e1226745.entities.persistence.Jockey;
import sepm.ws16.e1226745.entities.search.JockeySearch;
import sepm.ws16.e1226745.exceptions.DaoException;
import sepm.ws16.e1226745.persistence.JockeyDao;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by chop91 on 27.10.16.
 */
public class AbstractJockeyDaoTest {

    private JockeyDao jockeyDao;
    private Jockey existingJockey;
    private Jockey validJockey;
    private Jockey invalidJockey;

    protected void setJockeyDao(JockeyDao jockeyDao) {
        this.jockeyDao = jockeyDao;
    }

    protected void setExistingJockey(Jockey existingJockey) {
        this.existingJockey = existingJockey;
    }

    protected void setValidJockey(Jockey validJockey) {
        this.validJockey = validJockey;
    }

    protected void setInvalidJockey(Jockey invalidJockey) {
        this.invalidJockey = invalidJockey;
    }


    // Create
    @Test(expected = DaoException.class)
    public void createWithNullShouldThrowException() throws DaoException {
        jockeyDao.create(null);
    }

    @Test(expected = DaoException.class)
    public void createWithInvalidAgeShouldThrowException() throws DaoException {
        jockeyDao.create(invalidJockey);
    }

    @Test
    public void createWithValidParametersShouldPersist() throws DaoException {
        List<Jockey> jockeys = jockeyDao.readAllUndeleted();
        assertFalse(jockeys.contains(validJockey));
        jockeyDao.create(validJockey);
        jockeys = jockeyDao.readAllUndeleted();
        assertTrue(jockeys.contains(validJockey));
    }


    // Read
    @Test(expected = DaoException.class)
    public void readWithNullShouldThrowException() throws DaoException {
        jockeyDao.read(null);
    }

    @Test
    public void readWithValidParametersShouldReturnAll() throws DaoException {
        List<Jockey> jockeys = jockeyDao.readAllUndeleted();
        assertEquals(jockeys.size(), 10);
    }


    // Update
    @Test(expected = DaoException.class)
    public void updateWithInvalidIdShouldThrowException() throws DaoException {
        jockeyDao.update(invalidJockey);
    }

    @Test
    public void updateWithValidParametersShouldUpdate() throws DaoException {
        assertFalse(existingJockey.getFirstName().equals("New FirstName"));
        existingJockey.setFirstName("New FirstName");
        jockeyDao.update(existingJockey);
        JockeySearch validJockeySearch = new JockeySearch(existingJockey.getId(), existingJockey.getFirstName(), existingJockey.getLastName(), existingJockey.getAge(), existingJockey.getAge(), existingJockey.getSkill(), existingJockey.getSkill(), false);
        List<Jockey> jockeys = jockeyDao.read(validJockeySearch);
        assertTrue(jockeys.get(jockeys.indexOf(existingJockey)).getFirstName().equals("New FirstName"));
    }


    // Delete
    @Test(expected = DaoException.class)
    public void deleteWithInvalidIdShouldThrowException() throws DaoException {
        jockeyDao.delete(invalidJockey);
    }

    @Test
    public void deleteWithValidParametersShouldDelete() throws DaoException {
        List<Jockey> jockeys = jockeyDao.readAllUndeleted();
        assertTrue(jockeys.contains(existingJockey));
        jockeyDao.delete(existingJockey);
        jockeys = jockeyDao.readAllUndeleted();
        assertFalse(jockeys.contains(existingJockey));
    }
}
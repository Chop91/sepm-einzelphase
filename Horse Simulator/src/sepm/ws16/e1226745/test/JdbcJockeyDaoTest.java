package sepm.ws16.e1226745.test;

import org.h2.tools.RunScript;
import org.junit.After;
import org.junit.Before;
import sepm.ws16.e1226745.entities.persistence.Jockey;
import sepm.ws16.e1226745.persistence.JdbcJockeyDao;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by chop91 on 27.10.16.
 */
public class JdbcJockeyDaoTest extends AbstractJockeyDaoTest {

    private Connection connection;

    @Before
    public void setUp() throws SQLException, FileNotFoundException {
        connection = DriverManager.getConnection("jdbc:h2:mem:testcase;", "sa", null);
        connection.setAutoCommit(false);
        RunScript.execute(connection, new FileReader("./res/sql/create.sql"));
        RunScript.execute(connection, new FileReader("./res/sql/insert.sql"));
        connection.commit();

        JdbcJockeyDao jockeyDao = new JdbcJockeyDao(connection);
        setJockeyDao(jockeyDao);
        setExistingJockey(new Jockey(1, "Donald", "Trump", 18, -9.0, "res/images/trump.png", false));
        setValidJockey(new Jockey(11, "Donald Duplicate", "Trump", 18, -9.0, "res/images/trump.png", false));
        setInvalidJockey(new Jockey(-42, "No", "Jockey", -5, Double.NaN, "res/images/jockey-1.png", false));
    }

    @After
    public void tearDown() throws SQLException {
        connection.close();
    }
}
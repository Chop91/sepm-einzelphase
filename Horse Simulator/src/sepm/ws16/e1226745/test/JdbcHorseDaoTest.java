package sepm.ws16.e1226745.test;

import org.h2.tools.RunScript;
import org.junit.After;
import org.junit.Before;
import sepm.ws16.e1226745.entities.persistence.Horse;
import sepm.ws16.e1226745.persistence.HorseDao;
import sepm.ws16.e1226745.persistence.JdbcHorseDao;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by chop91 on 27.10.16.
 */
public class JdbcHorseDaoTest extends AbstractHorseDaoTest {

    private Connection connection;

    @Before
    public void setUp() throws SQLException, FileNotFoundException {
        connection = DriverManager.getConnection("jdbc:h2:mem:testcase;", "sa", null);
        connection.setAutoCommit(false);
        RunScript.execute(connection, new FileReader("./res/sql/create.sql"));
        RunScript.execute(connection, new FileReader("./res/sql/insert.sql"));
        connection.commit();

        HorseDao horseDao = new JdbcHorseDao(connection);
        setHorseDao(horseDao);
        setExistingHorse(new Horse(1, "Rainbow Dash", 5, 50.0, 60.0, "res/images/rainbow-dash.png", false));
        setValidHorse(new Horse(11, "Rainbow Dash Duplicate", 5, 50.0, 60.0, "res/images/rainbow-dash.png", false));
        setInvalidHorse(new Horse(-42, "No Pony", -5, 20.0, 80.0, "res/images/no-pony.png", false));
    }

    @After
    public void tearDown() throws SQLException {
        connection.close();
    }
}
package sepm.ws16.e1226745.test;

import org.junit.After;
import org.junit.Before;
import sepm.ws16.e1226745.entities.persistence.Horse;
import sepm.ws16.e1226745.entities.persistence.Jockey;
import sepm.ws16.e1226745.exceptions.ServiceException;
import sepm.ws16.e1226745.service.SimpleService;

/**
 * Created by Chop91 on 02.11.2016.
 */
public class SimpleServiceTest extends AbstractServiceTest {

    @Before
    public void setUp() throws ServiceException {
        service = new SimpleService();
        setValidHorse(new Horse(11, "Rainbow Dash Duplicate", 5, 50.0, 60.0, "res/images/rainbow-dash.png", false));
        setInvalidHorse(new Horse(-42, "No Pony", -5, 20.0, 80.0, "res/images/no-pony.png", false));
        setValidJockey(new Jockey(11, "Donald Duplicate", "Trump", 18, -9.0, "res/images/trump.png", false));
        setInvalidJockey(new Jockey(-42, "No", "Jockey", -5, Double.NaN, "res/images/jockey-1.png", false));
    }

    @After
    public void tearDown() throws ServiceException {
        service.closeService();
    }
}
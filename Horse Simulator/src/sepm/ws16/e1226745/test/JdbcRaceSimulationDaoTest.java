package sepm.ws16.e1226745.test;

import org.h2.tools.RunScript;
import org.junit.After;
import org.junit.Before;
import sepm.ws16.e1226745.entities.persistence.RaceSimulationEntry;
import sepm.ws16.e1226745.persistence.JdbcRaceSimulationDao;
import sepm.ws16.e1226745.persistence.RaceSimulationDao;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Chop91 on 02.11.2016.
 */
public class JdbcRaceSimulationDaoTest extends AbstractRaceSimulationDaoTest {

    private Connection connection;

    @Before
    public void setUp() throws SQLException, FileNotFoundException {
        connection = DriverManager.getConnection("jdbc:h2:mem:testcase;", "sa", null);
        connection.setAutoCommit(false);
        RunScript.execute(connection, new FileReader("./res/sql/create.sql"));
        RunScript.execute(connection, new FileReader("./res/sql/insert.sql"));
        connection.commit();

        RaceSimulationDao raceSimulationDao = new JdbcRaceSimulationDao(connection);
        setRaceSimulationDao(raceSimulationDao);
        setValidRaceSimulationEntry(new RaceSimulationEntry(25, 1, "Rainbow Dash", 1, "Donald Trump", 60.0, 50.0, 1.0, 1.0, 8));
    }

    @After
    public void tearDown() throws SQLException {
        connection.close();
    }
}
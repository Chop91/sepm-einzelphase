package sepm.ws16.e1226745.test;

import org.junit.Test;
import sepm.ws16.e1226745.entities.persistence.Horse;
import sepm.ws16.e1226745.entities.persistence.Jockey;
import sepm.ws16.e1226745.exceptions.ServiceException;
import sepm.ws16.e1226745.service.Service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Chop91 on 02.11.2016.
 */
public class AbstractServiceTest {

    protected Service service;
    private Horse validHorse;
    private Horse invalidHorse;
    private Jockey validJockey;
    private Jockey invalidJockey;

    protected void setValidHorse(Horse validHorse) {
        this.validHorse = validHorse;
    }

    protected void setInvalidHorse(Horse invalidHorse) {
        this.invalidHorse = invalidHorse;
    }

    protected void setValidJockey(Jockey validJockey) {
        this.validJockey = validJockey;
    }

    protected void setInvalidJockey(Jockey invalidJockey) {
        this.invalidJockey = invalidJockey;
    }

    @Test
    public void addValidHorseJockeyPairToRaceSimulation() throws ServiceException {
        service.addHorseJockeyPairToRaceSimulation(validHorse, validJockey);
        assertTrue(service.getCurrentCompetitors().containsKey(validHorse) && service.getCurrentCompetitors().get(validHorse).equals(validJockey));
    }

    @Test(expected = ServiceException.class)
    public void addInvalidHorseJockeyPairToRaceSimulationShouldThrowException() throws ServiceException {
        service.addHorseJockeyPairToRaceSimulation(invalidHorse, invalidJockey);
    }

    @Test
    public void removeValidHorseJockeyPairFromRaceSimulation() throws ServiceException {
        service.addHorseJockeyPairToRaceSimulation(validHorse, validJockey);
        assertTrue(service.getCurrentCompetitors().containsKey(validHorse) && service.getCurrentCompetitors().get(validHorse).equals(validJockey));
        service.removeHorseJockeyPairFromRaceSimulation(validHorse, validJockey);
        assertFalse(service.getCurrentCompetitors().containsKey(validHorse) && service.getCurrentCompetitors().get(validHorse).equals(validJockey));
    }

    @Test(expected = ServiceException.class)
    public void removeInvalidHorseJockeyPairFromRaceSimulationShouldThrowException() throws ServiceException {
        service.addHorseJockeyPairToRaceSimulation(validHorse, validJockey);
        service.removeHorseJockeyPairFromRaceSimulation(invalidHorse, invalidJockey);
    }

    @Test(expected = ServiceException.class)
    public void removeHorseJockeyPairFromEmptyRaceSimulationShouldThrowException() throws ServiceException {
        service.removeHorseJockeyPairFromRaceSimulation(validHorse, validJockey);
    }
}
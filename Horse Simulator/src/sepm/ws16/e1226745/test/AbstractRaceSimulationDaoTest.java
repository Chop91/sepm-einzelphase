package sepm.ws16.e1226745.test;

import org.junit.Test;
import sepm.ws16.e1226745.entities.persistence.RaceSimulationEntry;
import sepm.ws16.e1226745.entities.search.RaceSimulationSearch;
import sepm.ws16.e1226745.exceptions.DaoException;
import sepm.ws16.e1226745.persistence.RaceSimulationDao;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Chop91 on 02.11.2016.
 */
public class AbstractRaceSimulationDaoTest {

    private RaceSimulationDao raceSimulationDao;
    private RaceSimulationEntry validRaceSimulationEntry;

    protected void setRaceSimulationDao(RaceSimulationDao raceSimulationDao) {
        this.raceSimulationDao = raceSimulationDao;
    }

    protected void setValidRaceSimulationEntry(RaceSimulationEntry validRaceSimulationEntry) {
        this.validRaceSimulationEntry = validRaceSimulationEntry;
    }


    // Create
    @Test(expected = DaoException.class)
    public void createWithNullShouldThrowException() throws DaoException {
        raceSimulationDao.create(null);
    }

    @Test
    public void createWithValidParametersShouldPersist() throws DaoException {
        List<RaceSimulationEntry> raceSimulationEntries = raceSimulationDao.read(new RaceSimulationSearch());
        assertFalse(raceSimulationEntries.contains(validRaceSimulationEntry));
        ArrayList<RaceSimulationEntry> list = new ArrayList<>();
        list.add(validRaceSimulationEntry);
        raceSimulationDao.create(list);
        raceSimulationEntries = raceSimulationDao.read(new RaceSimulationSearch());
        assertTrue(raceSimulationEntries.contains(validRaceSimulationEntry));
    }


    // Read
    @Test(expected = DaoException.class)
    public void readWithNullShouldThrowException() throws DaoException {
        raceSimulationDao.read(null);
    }

    @Test
    public void readWithValidParametersShouldReturnAll() throws DaoException {
        List<RaceSimulationEntry> raceSimulationEntries = raceSimulationDao.read(new RaceSimulationSearch());
        assertEquals(raceSimulationEntries.size(), 42);
    }
}
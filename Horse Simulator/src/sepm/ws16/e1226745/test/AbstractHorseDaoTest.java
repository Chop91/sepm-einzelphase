package sepm.ws16.e1226745.test;

import org.junit.Test;
import sepm.ws16.e1226745.entities.persistence.Horse;
import sepm.ws16.e1226745.entities.search.HorseSearch;
import sepm.ws16.e1226745.exceptions.DaoException;
import sepm.ws16.e1226745.persistence.HorseDao;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by chop91 on 27.10.16.
 */
public abstract class AbstractHorseDaoTest {

    private HorseDao horseDao;
    private Horse existingHorse;
    private Horse validHorse;
    private Horse invalidHorse;

    protected void setHorseDao(HorseDao horseDao) {
        this.horseDao = horseDao;
    }

    protected void setExistingHorse(Horse existingHorse) {
        this.existingHorse = existingHorse;
    }

    protected void setValidHorse(Horse validHorse) {
        this.validHorse = validHorse;
    }

    protected void setInvalidHorse(Horse invalidHorse) {
        this.invalidHorse = invalidHorse;
    }


    // Create
    @Test(expected = DaoException.class)
    public void createWithNullShouldThrowException() throws DaoException {
        horseDao.create(null);
    }

    @Test(expected = DaoException.class)
    public void createWithInvalidSpeedShouldThrowException() throws DaoException {
        horseDao.create(invalidHorse);
    }

    @Test
    public void createWithValidParametersShouldPersist() throws DaoException {
        List<Horse> horses = horseDao.readAllUndeleted();
        assertFalse(horses.contains(validHorse));
        horseDao.create(validHorse);
        horses = horseDao.readAllUndeleted();
        assertTrue(horses.contains(validHorse));
    }


    // Read
    @Test(expected = DaoException.class)
    public void readWithNullShouldThrowException() throws DaoException {
        horseDao.read(null);
    }

    @Test
    public void readWithValidParametersShouldReturnAll() throws DaoException {
        List<Horse> horses = horseDao.readAllUndeleted();
        assertEquals(horses.size(), 10);
    }


    // Update
    @Test(expected = DaoException.class)
    public void updateWithInvalidIdShouldThrowException() throws DaoException {
        horseDao.update(invalidHorse);
    }

    @Test
    public void updateWithValidParametersShouldUpdate() throws DaoException {
        assertFalse(existingHorse.getName().equals("New Name"));
        existingHorse.setName("New Name");
        horseDao.update(existingHorse);
        HorseSearch validHorseSearch = new HorseSearch(existingHorse.getId(), existingHorse.getName(), existingHorse.getAge(), existingHorse.getAge(), existingHorse.getMinSpeed(), existingHorse.getMinSpeed(), existingHorse.getMaxSpeed(), existingHorse.getMaxSpeed(), false);
        List<Horse> horses = horseDao.read(validHorseSearch);
        assertTrue(horses.get(horses.indexOf(existingHorse)).getName().equals("New Name"));
    }


    // Delete
    @Test(expected = DaoException.class)
    public void deleteWithInvalidIdShouldThrowException() throws DaoException {
        horseDao.delete(invalidHorse);
    }

    @Test
    public void deleteWithValidParametersShouldDelete() throws DaoException {
        List<Horse> horses = horseDao.readAllUndeleted();
        assertTrue(horses.contains(existingHorse));
        horseDao.delete(existingHorse);
        horses = horseDao.readAllUndeleted();
        assertFalse(horses.contains(existingHorse));
    }
}
package sepm.ws16.e1226745.entities.persistence;

/**
 * Created by chop91 on 08.10.16.
 */
public class Horse {
    private Integer id;
    private String name;
    private Integer age;
    private Double minSpeed;
    private Double maxSpeed;
    private String imagePath;
    private Boolean isDeleted;

    public Horse(Integer id, String name, Integer age, Double minSpeed, Double maxSpeed, String imagePath, Boolean isDeleted) {
        this.setId(id);
        this.setName(name);
        this.setAge(age);
        this.setMinSpeed(minSpeed);
        this.setMaxSpeed(maxSpeed);
        this.setImagePath(imagePath);
        this.setDeleted(isDeleted);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Double getMinSpeed() {
        return minSpeed;
    }

    public void setMinSpeed(Double minSpeed) {
        this.minSpeed = minSpeed;
    }

    public Double getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(Double maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if ((object == null) || (object.getClass() != getClass())) {
            return false;
        }

        Horse other = (Horse) object;
        if (getId() == null || other.getId() == null) {
            return false;
        }
        return other.getId().equals(getId());
    }

    @Override
    public int hashCode() {
        if (getId() == null) {
            return super.hashCode();
        }
        return id;
    }

    @Override
    public String toString() {
        return String.format("Horse[id:%d, name:%s, age:%d, minSpeed:%f, maxSpeed:%f, imagePath:%s, isDeleted:%b]", id, name, age, minSpeed, maxSpeed, imagePath, isDeleted);
    }
}
package sepm.ws16.e1226745.entities.persistence;

/**
 * Created by chop91 on 08.10.16.
 */
public class Jockey {
    private Integer id;
    private String firstName;
    private String lastName;
    private Integer age;
    private Double skill;
    private String imagePath;
    private Boolean isDeleted;

    public Jockey(Integer id, String firstName, String lastName, Integer age, Double skill, String imagePath, Boolean isDeleted) {
        this.setId(id);
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setAge(age);
        this.setSkill(skill);
        this.setImagePath(imagePath);
        this.setDeleted(isDeleted);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Double getSkill() {
        return skill;
    }

    public void setSkill(Double skill) {
        this.skill = skill;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    @Override
    public int hashCode() {
        if (getId() == null) {
            return super.hashCode();
        }
        return id;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if ((object == null) || (object.getClass() != getClass())) {
            return false;
        }

        Jockey other = (Jockey) object;
        if (getId() == null || other.getId() == null) {
            return false;
        }
        return other.getId().equals(getId());
    }

    @Override
    public String toString() {
        return String.format("Jockey[id:%d, firstName:%s, lastName:%s, age:%d, skill:%f, imagePath:%s, isDeleted:%b]", id, firstName, lastName, age, skill, imagePath, isDeleted);
    }
}
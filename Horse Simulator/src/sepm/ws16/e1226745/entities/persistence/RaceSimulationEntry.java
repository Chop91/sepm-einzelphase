package sepm.ws16.e1226745.entities.persistence;

/**
 * Created by chop91 on 08.10.16.
 */
public class RaceSimulationEntry implements Comparable<RaceSimulationEntry> {
    private Integer id;
    private Integer horseId;
    private String horseName;
    private Integer jockeyId;
    private String jockeyName;
    private Double averageSpeed;
    private Double randomSpeed;
    private Double luckFactor;
    private Double calculatedSkill;
    private Integer rank;

    public RaceSimulationEntry(Integer id, Integer horseId, String horseName, Integer jockeyId, String jockeyName, Double averageSpeed, Double randomSpeed, Double luckFactor, Double calculatedSkill, Integer rank) {
        this.setId(id);
        this.setHorseId(horseId);
        this.setHorseName(horseName);
        this.setJockeyId(jockeyId);
        this.setJockeyName(jockeyName);
        this.setAverageSpeed(averageSpeed);
        this.setRandomSpeed(randomSpeed);
        this.setLuckFactor(luckFactor);
        this.setCalculatedSkill(calculatedSkill);
        this.setRank(rank);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getHorseId() {
        return horseId;
    }

    public void setHorseId(Integer horseId) {
        this.horseId = horseId;
    }

    public String getHorseName() {
        return horseName;
    }

    public void setHorseName(String horseName) {
        this.horseName = horseName;
    }

    public Integer getJockeyId() {
        return jockeyId;
    }

    public void setJockeyId(Integer jockeyId) {
        this.jockeyId = jockeyId;
    }

    public String getJockeyName() {
        return jockeyName;
    }

    public void setJockeyName(String jockeyName) {
        this.jockeyName = jockeyName;
    }

    public Double getAverageSpeed() {
        return averageSpeed;
    }

    public void setAverageSpeed(Double averageSpeed) {
        this.averageSpeed = averageSpeed;
    }

    public Double getRandomSpeed() {
        return randomSpeed;
    }

    public void setRandomSpeed(Double randomSpeed) {
        this.randomSpeed = randomSpeed;
    }

    public Double getLuckFactor() {
        return luckFactor;
    }

    public void setLuckFactor(Double luckFactor) {
        this.luckFactor = luckFactor;
    }

    public Double getCalculatedSkill() {
        return calculatedSkill;
    }

    public void setCalculatedSkill(Double calculatedSkill) {
        this.calculatedSkill = calculatedSkill;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    @Override
    public int hashCode() {
        if (getId() == null || getHorseId() == null || getJockeyId() == null) {
            return super.hashCode();
        }
        return 10000 * id + 100 * horseId + jockeyId;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if ((object == null) || (object.getClass() != getClass())) {
            return false;
        }

        RaceSimulationEntry other = (RaceSimulationEntry) object;
        if (getId() == null || other.getId() == null || getHorseId() == null || other.getHorseId() == null || getJockeyId() == null || other.getJockeyId() == null) {
            return false;
        }
        return other.getId().equals(getId()) && other.getHorseId().equals(getHorseId()) && other.getJockeyId().equals(getJockeyId());
    }

    @Override
    public String toString() {
        return String.format("RaceSimulationEntry[id:%d, horseId:%d, horseName:%s, jockeyId:%d, jockeyName:%s, averageSpeed:%f, randomSpeed:%f, luckFactor:%f, calulatedSkill:%f, rank:%d]", id, horseId, horseName, jockeyId, jockeyName, averageSpeed, randomSpeed, luckFactor, calculatedSkill, rank);
    }

    @Override
    public int compareTo(RaceSimulationEntry o) {
        // Inverted for descending order
        return -this.averageSpeed.compareTo(o.averageSpeed);
    }
}
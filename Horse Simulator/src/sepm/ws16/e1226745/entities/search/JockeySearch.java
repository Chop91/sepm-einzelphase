package sepm.ws16.e1226745.entities.search;

/**
 * Created by chop91 on 29.10.16.
 */
public class JockeySearch {
    private Integer id;
    private String firstName;
    private String lastName;
    private Integer lowerAge;
    private Integer upperAge;
    private Double lowerSkill;
    private Double upperSkill;
    private Boolean jockeyIsDeleted;

    public JockeySearch() {
    }

    public JockeySearch(Integer id, Boolean jockeyIsDeleted) {
        this.setId(id);
        this.setJockeyIsDeleted(jockeyIsDeleted);
    }

    public JockeySearch(Integer id, String firstName, String lastName, Integer lowerAge, Integer upperAge, Double lowerSkill, Double upperSkill, Boolean jockeyIsDeleted) {
        this.setId(id);
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setLowerAge(lowerAge);
        this.setUpperAge(upperAge);
        this.setLowerSkill(lowerSkill);
        this.setUpperSkill(upperSkill);
        this.setJockeyIsDeleted(jockeyIsDeleted);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getLowerAge() {
        return lowerAge;
    }

    public void setLowerAge(Integer lowerAge) {
        this.lowerAge = lowerAge;
    }

    public Integer getUpperAge() {
        return upperAge;
    }

    public void setUpperAge(Integer upperAge) {
        this.upperAge = upperAge;
    }

    public Double getLowerSkill() {
        return lowerSkill;
    }

    public void setLowerSkill(Double lowerSkill) {
        this.lowerSkill = lowerSkill;
    }

    public Double getUpperSkill() {
        return upperSkill;
    }

    public void setUpperSkill(Double upperSkill) {
        this.upperSkill = upperSkill;
    }

    public Boolean getJockeyIsDeleted() {
        return jockeyIsDeleted;
    }

    public void setJockeyIsDeleted(Boolean jockeyIsDeleted) {
        this.jockeyIsDeleted = jockeyIsDeleted;
    }

    @Override
    public String toString() {
        return String.format("JockeySearch[id:%d, firstName:%s, lastName:%s, lowerAge:%d, upperAge:%d, lowerSkill:%f, upperSkill:%f, isDeleted:%b]", id, firstName, lastName, lowerAge, upperAge, lowerSkill, upperSkill, jockeyIsDeleted);
    }
}
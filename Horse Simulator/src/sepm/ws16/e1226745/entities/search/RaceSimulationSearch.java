package sepm.ws16.e1226745.entities.search;

/**
 * Created by chop91 on 29.10.16.
 */
public class RaceSimulationSearch {
    private Integer id;
    private Integer horseId;
    private Integer jockeyId;

    public RaceSimulationSearch() {
    }

    public RaceSimulationSearch(Integer id, Integer horseId, Integer jockeyId) {
        this.setId(id);
        this.setHorseId(horseId);
        this.setJockeyId(jockeyId);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getHorseId() {
        return horseId;
    }

    public void setHorseId(Integer horseId) {
        this.horseId = horseId;
    }

    public Integer getJockeyId() {
        return jockeyId;
    }

    public void setJockeyId(Integer jockeyId) {
        this.jockeyId = jockeyId;
    }

    @Override
    public String toString() {
        return String.format("RaceSimulationSearch[id:%d, horseId:%d, jockeyId:%d]", id, horseId, jockeyId);
    }
}
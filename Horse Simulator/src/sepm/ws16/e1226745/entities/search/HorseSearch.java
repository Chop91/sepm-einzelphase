package sepm.ws16.e1226745.entities.search;

/**
 * Created by chop91 on 28.10.16.
 */
public class HorseSearch {
    private Integer id;
    private String name;
    private Integer lowerAge;
    private Integer upperAge;
    private Double lowerMinSpeed;
    private Double upperMinSpeed;
    private Double lowerMaxSpeed;
    private Double upperMaxSpeed;
    private Boolean horseIsDeleted;

    public HorseSearch() {
    }

    public HorseSearch(Integer id, Boolean horseIsDeleted) {
        this.setId(id);
        this.setHorseIsDeleted(horseIsDeleted);
    }

    public HorseSearch(Integer id, String name, Integer lowerAge, Integer upperAge, Double lowerMinSpeed, Double upperMinSpeed, Double lowerMaxSpeed, Double upperMaxSpeed, Boolean horseIsDeleted) {
        this.setId(id);
        this.setName(name);
        this.setLowerAge(lowerAge);
        this.setUpperAge(upperAge);
        this.setLowerMinSpeed(lowerMinSpeed);
        this.setUpperMinSpeed(upperMinSpeed);
        this.setLowerMaxSpeed(lowerMaxSpeed);
        this.setUpperMaxSpeed(upperMaxSpeed);
        this.setHorseIsDeleted(horseIsDeleted);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLowerAge() {
        return lowerAge;
    }

    public void setLowerAge(Integer lowerAge) {
        this.lowerAge = lowerAge;
    }

    public Integer getUpperAge() {
        return upperAge;
    }

    public void setUpperAge(Integer upperAge) {
        this.upperAge = upperAge;
    }

    public Double getLowerMinSpeed() {
        return lowerMinSpeed;
    }

    public void setLowerMinSpeed(Double lowerMinSpeed) {
        this.lowerMinSpeed = lowerMinSpeed;
    }

    public Double getUpperMinSpeed() {
        return upperMinSpeed;
    }

    public void setUpperMinSpeed(Double upperMinSpeed) {
        this.upperMinSpeed = upperMinSpeed;
    }

    public Double getLowerMaxSpeed() {
        return lowerMaxSpeed;
    }

    public void setLowerMaxSpeed(Double lowerMaxSpeed) {
        this.lowerMaxSpeed = lowerMaxSpeed;
    }

    public Double getUpperMaxSpeed() {
        return upperMaxSpeed;
    }

    public void setUpperMaxSpeed(Double upperMaxSpeed) {
        this.upperMaxSpeed = upperMaxSpeed;
    }

    public Boolean getHorseIsDeleted() {
        return horseIsDeleted;
    }

    public void setHorseIsDeleted(Boolean horseIsDeleted) {
        this.horseIsDeleted = horseIsDeleted;
    }

    @Override
    public String toString() {
        return String.format("HorseSearch[id:%d, name:%s, lowerAge:%d, upperAge:%d, lowerMinSpeed:%f, upperMinSpeed:%f, lowerMaxSpeed:%f, upperMaxSpeed:%f, isDeleted:%b]", id, name, lowerAge, upperAge, lowerMinSpeed, upperMinSpeed, lowerMaxSpeed, upperMaxSpeed, horseIsDeleted);
    }
}
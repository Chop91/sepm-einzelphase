package sepm.ws16.e1226745.gui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.Tooltip;
import javafx.util.converter.DoubleStringConverter;
import javafx.util.converter.IntegerStringConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sepm.ws16.e1226745.entities.persistence.Jockey;


/**
 * Created by Chop91 on 31.10.2016.
 */
public class JockeyCreateUpdateViewController {

    private static final Logger logger = LoggerFactory.getLogger(JockeyCreateUpdateViewController.class);

    @FXML
    private JockeysTabViewController jockeysTabViewController;

    private Jockey jockeyNew;

    private Jockey jockey;

    private String imagePath;

    @FXML
    private TextField textFieldJockeyFormFirstName;

    @FXML
    private TextField textFieldJockeyFormLastName;

    @FXML
    private TextField textFieldJockeyFormAge;

    @FXML
    private TextField textFieldJockeyFormSkill;

    @FXML
    private Label labelJockeyFormImagePath;

    @FXML
    void buttonJockeyFormSelectImage_OnAction(ActionEvent event) {
        logger.debug("buttonJockeyFormSelectImage_OnAction");
        imagePath = jockeysTabViewController.getMainViewController().selectImageFile(jockeysTabViewController.getStage());
        if (imagePath != null) {
            labelJockeyFormImagePath.setText(String.format("'%s'", imagePath));
            labelJockeyFormImagePath.setTooltip(new Tooltip(imagePath));
        }
    }

    @FXML
    void buttonJockeyFormSubmit_OnAction(ActionEvent event) {
        logger.debug("buttonJockeyFormSubmit_OnAction");
        Integer id = jockey == null ? null : jockey.getId();
        String firstName = textFieldJockeyFormFirstName.getText() == null || textFieldJockeyFormFirstName.getText().isEmpty() ? null : textFieldJockeyFormFirstName.getText();
        String lastName = textFieldJockeyFormLastName.getText() == null || textFieldJockeyFormLastName.getText().isEmpty() ? null : textFieldJockeyFormLastName.getText();
        Integer age = textFieldJockeyFormAge.getText() == null || textFieldJockeyFormAge.getText().isEmpty() ? null : Integer.valueOf(textFieldJockeyFormAge.getText());
        Double skill = textFieldJockeyFormSkill.getText() == null || textFieldJockeyFormSkill.getText().isEmpty() ? null : Double.valueOf(textFieldJockeyFormSkill.getText());
        Boolean isDeleted = jockey == null ? false : jockey.getDeleted();

        jockeyNew = new Jockey(id, firstName, lastName, age, skill, imagePath, isDeleted);
        jockeysTabViewController.getStage().close();
        logger.debug("'Jockey' {} submitted.", jockeyNew);
    }

    @FXML
    void buttonHorseFormCancel_OnAction(ActionEvent event) {
        logger.debug("buttonHorseFormCancel_OnAction");
        jockeysTabViewController.getStage().close();
    }

    /**
     * Initializes the controller.
     *
     * @param jockeysTabViewController The parent controller.
     */
    public void initialize(JockeysTabViewController jockeysTabViewController) {
        logger.debug("initialize");
        this.jockeysTabViewController = jockeysTabViewController;

        textFieldJockeyFormAge.setTextFormatter(new TextFormatter<>(new IntegerStringConverter()));
        textFieldJockeyFormSkill.setTextFormatter(new TextFormatter<>(new DoubleStringConverter()));

        Tooltip tooltipAge = new Tooltip("Enter a positive integer (e.g. '42').");
        Tooltip tooltipSkill = new Tooltip("Enter a decimal number (e.g. '-4.2').");
        textFieldJockeyFormAge.setTooltip(tooltipAge);
        textFieldJockeyFormSkill.setTooltip(tooltipSkill);

        if (jockey != null) {
            textFieldJockeyFormFirstName.setText(jockey.getFirstName());
            textFieldJockeyFormLastName.setText(jockey.getLastName());
            textFieldJockeyFormAge.setText(String.valueOf(jockey.getAge()));
            textFieldJockeyFormSkill.setText(String.valueOf(jockey.getSkill()));
            labelJockeyFormImagePath.setText(String.format("'%s'", jockey.getImagePath()));
            imagePath = jockey.getImagePath();
        }
    }

    public Jockey getJockeyNew() {
        return jockeyNew;
    }

    public Jockey getJockey() {
        return jockey;
    }

    public void setJockey(Jockey jockey) {
        this.jockey = jockey;
    }
}
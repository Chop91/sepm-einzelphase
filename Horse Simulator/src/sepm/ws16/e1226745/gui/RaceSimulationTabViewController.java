package sepm.ws16.e1226745.gui;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.converter.IntegerStringConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sepm.ws16.e1226745.entities.persistence.RaceSimulationEntry;
import sepm.ws16.e1226745.entities.search.RaceSimulationSearch;
import sepm.ws16.e1226745.exceptions.ServiceException;

import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Chop91 on 30.10.2016.
 */
public class RaceSimulationTabViewController {

    private static final Logger logger = LoggerFactory.getLogger(RaceSimulationTabViewController.class);

    @FXML
    private MainViewController mainViewController;

    @FXML
    private TableView<RaceSimulationEntry> tableViewRaceSimulation;

    @FXML
    private BarChart<String, Long> barChartRaceSimulation;

    @FXML
    private NumberAxis yAxis;

    @FXML
    private TextField textFieldRaceSimulationRaceId;

    @FXML
    private TextField textFieldRaceSimulationHorseId;

    @FXML
    private TextField textFieldRaceSimulationJockeyId;

    @FXML
    private TableColumn<RaceSimulationEntry, String> tableColumnRaceSimulationRaceId;

    @FXML
    private TableColumn<RaceSimulationEntry, String> tableColumnRaceSimulationHorseId;

    @FXML
    private TableColumn<RaceSimulationEntry, String> tableColumnRaceSimulationHorseName;

    @FXML
    private TableColumn<RaceSimulationEntry, String> tableColumnRaceSimulationJockeyId;

    @FXML
    private TableColumn<RaceSimulationEntry, String> tableColumnRaceSimulationJockeyName;

    @FXML
    private TableColumn<RaceSimulationEntry, String> tableColumnRaceSimulationAvgSpeed;

    @FXML
    private TableColumn<RaceSimulationEntry, String> tableColumnRaceSimulationRandSpeed;

    @FXML
    private TableColumn<RaceSimulationEntry, String> tableColumnRaceSimulationLuckFactor;

    @FXML
    private TableColumn<RaceSimulationEntry, String> tableColumnRaceSimulationCalcSkill;

    @FXML
    private TableColumn<RaceSimulationEntry, String> tableColumnRaceSimulationRank;

    @FXML
    void buttonRaceSimulationSearch_OnAction(ActionEvent event) {
        logger.debug("buttonRaceSimulationSearch_OnAction");
        try {
            Integer id = textFieldRaceSimulationRaceId.getText() == null || textFieldRaceSimulationRaceId.getText().isEmpty() ? null : Integer.valueOf(textFieldRaceSimulationRaceId.getText());
            Integer horseId = textFieldRaceSimulationHorseId.getText() == null || textFieldRaceSimulationHorseId.getText().isEmpty() ? null : Integer.valueOf(textFieldRaceSimulationHorseId.getText());
            Integer jockeyId = textFieldRaceSimulationJockeyId.getText() == null || textFieldRaceSimulationJockeyId.getText().isEmpty() ? null : Integer.valueOf(textFieldRaceSimulationJockeyId.getText());

            RaceSimulationSearch raceSimulationSearch = new RaceSimulationSearch(id, horseId, jockeyId);
            List<RaceSimulationEntry> raceSimulationEntries = mainViewController.getService().searchRaceSimulation(raceSimulationSearch);
            tableViewRaceSimulation.setItems(FXCollections.observableArrayList(raceSimulationEntries));
            tableViewRaceSimulation.getSelectionModel().selectFirst();

            if (raceSimulationEntries.isEmpty()) {
                Alert alert = new CustomAlert(Alert.AlertType.WARNING, "Search Failed", "Searching the race simulation entry failed, see details.", "No entries found.");
                alert.showAndWait();
            }

            if (horseId != null || jockeyId != null) {
                Map<Integer, Long> occurrences = mainViewController.getService().generateStatistics(raceSimulationSearch);

                if (!occurrences.isEmpty()) {
                    BarChart.Series<String, Long> series = new BarChart.Series();
                    Long maxValue = Long.MIN_VALUE;
                    for (Map.Entry<Integer, Long> entry : occurrences.entrySet()) {
                        series.getData().add(new BarChart.Data<>(String.valueOf(entry.getKey()), entry.getValue()));
                        maxValue = Math.max(maxValue, entry.getValue());
                    }

                    barChartRaceSimulation.getData().clear();
                    yAxis.setUpperBound(maxValue);
                    barChartRaceSimulation.getData().add(series);
                    barChartRaceSimulation.setVisible(true);
                } else {
                    barChartRaceSimulation.setVisible(false);
                }
            } else {
                barChartRaceSimulation.setVisible(false);
            }
        } catch (ServiceException e) {
            Alert alert = new CustomAlert(Alert.AlertType.WARNING, "Search Failed", "Searching the race simulation entry failed, see details.", e.getMessage());
            alert.showAndWait();
        }
    }

    /**
     * Refreshes the table view.
     */
    public void refreshTableView() {
        logger.debug("refreshTableView");
        try {
            RaceSimulationSearch raceSimulationSearch = new RaceSimulationSearch(null, null, null);
            List<RaceSimulationEntry> raceSimulationEntries = mainViewController.getService().searchRaceSimulation(raceSimulationSearch);
            tableViewRaceSimulation.setItems(FXCollections.observableArrayList(raceSimulationEntries));
            tableViewRaceSimulation.getSelectionModel().selectFirst();

            // FIX: There seems to be a bug with the scrollbar
            // https://stackoverflow.com/questions/37423748/javafx-tablecolumns-headers-not-aligned-with-cells-due-to-vertical-scrollbar
            Platform.runLater(() -> tableViewRaceSimulation.refresh());
        } catch (ServiceException e) {
            Alert alert = new CustomAlert(Alert.AlertType.WARNING, "Refreshing Table Failed", "Refreshing the table failed, see details.", e.getMessage());
            alert.showAndWait();
        }
    }

    /**
     * Initializes the controller.
     *
     * @param mainViewController The parent controller.
     */
    public void initialize(MainViewController mainViewController) {
        logger.debug("initialize");
        this.mainViewController = mainViewController;

        barChartRaceSimulation.setVisible(false);

        textFieldRaceSimulationRaceId.setTextFormatter(new TextFormatter<>(new IntegerStringConverter()));
        textFieldRaceSimulationHorseId.setTextFormatter(new TextFormatter<>(new IntegerStringConverter()));
        textFieldRaceSimulationJockeyId.setTextFormatter(new TextFormatter<>(new IntegerStringConverter()));

        Tooltip tooltipId = new Tooltip("Enter a positive integer (e.g. '42').");
        textFieldRaceSimulationRaceId.setTooltip(tooltipId);
        textFieldRaceSimulationHorseId.setTooltip(tooltipId);
        textFieldRaceSimulationJockeyId.setTooltip(tooltipId);

        tableColumnRaceSimulationRaceId.setCellValueFactory(new PropertyValueFactory<>("id"));
        tableColumnRaceSimulationHorseId.setCellValueFactory(new PropertyValueFactory<>("horseId"));
        tableColumnRaceSimulationHorseName.setCellValueFactory(new PropertyValueFactory<>("horseName"));
        tableColumnRaceSimulationJockeyId.setCellValueFactory(new PropertyValueFactory<>("jockeyId"));
        tableColumnRaceSimulationJockeyName.setCellValueFactory(new PropertyValueFactory<>("jockeyName"));
        tableColumnRaceSimulationAvgSpeed.setCellValueFactory(param -> Bindings.format(Locale.US, "%.2f", param.getValue().getAverageSpeed()));
        tableColumnRaceSimulationRandSpeed.setCellValueFactory(param -> Bindings.format(Locale.US, "%.2f", param.getValue().getRandomSpeed()));
        tableColumnRaceSimulationLuckFactor.setCellValueFactory(param -> Bindings.format(Locale.US, "%.2f", param.getValue().getLuckFactor()));
        tableColumnRaceSimulationCalcSkill.setCellValueFactory(param -> Bindings.format(Locale.US, "%.2f", param.getValue().getCalculatedSkill()));
        tableColumnRaceSimulationRank.setCellValueFactory(new PropertyValueFactory<>("rank"));
    }
}

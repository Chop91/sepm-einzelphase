package sepm.ws16.e1226745.gui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.Tooltip;
import javafx.util.converter.DoubleStringConverter;
import javafx.util.converter.IntegerStringConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sepm.ws16.e1226745.entities.persistence.Horse;

/**
 * Created by Chop91 on 31.10.2016.
 */
public class HorseCreateUpdateViewController {

    private static final Logger logger = LoggerFactory.getLogger(HorseCreateUpdateViewController.class);

    @FXML
    private HorsesTabViewController horsesTabViewController;

    private Horse horseNew;

    private Horse horse;

    private String imagePath;

    @FXML
    private TextField textFieldHorseFormName;

    @FXML
    private TextField textFieldHorseFormAge;

    @FXML
    private TextField textFieldHorseFormMinSpeed;

    @FXML
    private TextField textFieldHorseFormMaxSpeed;

    @FXML
    private Label labelHorseFormImagePath;

    @FXML
    void buttonHorseFormSelectImage_OnAction(ActionEvent event) {
        logger.debug("buttonHorseFormSelectImage_OnAction");
        imagePath = horsesTabViewController.getMainViewController().selectImageFile(horsesTabViewController.getStage());
        if (imagePath != null) {
            labelHorseFormImagePath.setText(String.format("'%s'", imagePath));
            labelHorseFormImagePath.setTooltip(new Tooltip(imagePath));
        }
    }

    @FXML
    void buttonHorseFormSubmit_OnAction(ActionEvent event) {
        logger.debug("buttonHorseFormSubmit_OnAction");
        Integer id = horse == null ? null : horse.getId();
        String name = textFieldHorseFormName.getText() == null || textFieldHorseFormName.getText().isEmpty() ? null : textFieldHorseFormName.getText();
        Integer age = textFieldHorseFormAge.getText() == null || textFieldHorseFormAge.getText().isEmpty() ? null : Integer.valueOf(textFieldHorseFormAge.getText());
        Double minSpeed = textFieldHorseFormMinSpeed.getText() == null || textFieldHorseFormMinSpeed.getText().isEmpty() ? null : Double.valueOf(textFieldHorseFormMinSpeed.getText());
        Double maxSpeed = textFieldHorseFormMaxSpeed.getText() == null || textFieldHorseFormMaxSpeed.getText().isEmpty() ? null : Double.valueOf(textFieldHorseFormMaxSpeed.getText());
        Boolean isDeleted = horse == null ? false : horse.getDeleted();

        horseNew = new Horse(id, name, age, minSpeed, maxSpeed, imagePath, isDeleted);
        horsesTabViewController.getStage().close();
        logger.debug("'Horse' {} submitted.", horseNew);
    }

    @FXML
    void buttonHorseFormCancel_OnAction(ActionEvent event) {
        logger.debug("buttonHorseFormCancel_OnAction");
        horsesTabViewController.getStage().close();
    }

    /**
     * Initializes the controller.
     *
     * @param horsesTabViewController The parent controller.
     */
    public void initialize(HorsesTabViewController horsesTabViewController) {
        logger.debug("initialize");
        this.horsesTabViewController = horsesTabViewController;

        textFieldHorseFormAge.setTextFormatter(new TextFormatter<>(new IntegerStringConverter()));
        textFieldHorseFormMinSpeed.setTextFormatter(new TextFormatter<>(new DoubleStringConverter()));
        textFieldHorseFormMaxSpeed.setTextFormatter(new TextFormatter<>(new DoubleStringConverter()));

        Tooltip tooltipAge = new Tooltip("Enter a positive integer (e.g. '42').");
        Tooltip tooltipSpeed = new Tooltip("Enter a decimal number (e.g. '-4.2').");
        textFieldHorseFormAge.setTooltip(tooltipAge);
        textFieldHorseFormMinSpeed.setTooltip(tooltipSpeed);
        textFieldHorseFormMaxSpeed.setTooltip(tooltipSpeed);

        if (horse != null) {
            textFieldHorseFormName.setText(horse.getName());
            textFieldHorseFormAge.setText(String.valueOf(horse.getAge()));
            textFieldHorseFormMinSpeed.setText(String.valueOf(horse.getMinSpeed()));
            textFieldHorseFormMaxSpeed.setText(String.valueOf(horse.getMaxSpeed()));
            labelHorseFormImagePath.setText(String.format("'%s'", horse.getImagePath()));
            imagePath = horse.getImagePath();
        }
    }

    public Horse getHorseNew() {
        return horseNew;
    }

    public Horse getHorse() {
        return horse;
    }

    public void setHorse(Horse horse) {
        this.horse = horse;
    }
}
package sepm.ws16.e1226745.gui;

/**
 * Created by Chop91 on 30.10.2016.
 */

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main extends Application {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    @FXML
    private MainViewController mainViewController;

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("MainView.fxml"));
        Parent root = fxmlLoader.load();
        primaryStage.setTitle("Horse Simulator");
        primaryStage.setScene(new Scene(root));

        mainViewController = fxmlLoader.getController();
        mainViewController.start(primaryStage);

        primaryStage.show();

        logger.info("Application started.");
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        mainViewController.stop();
        logger.info("Application stopped.");
    }

    public static void main(String[] args) {
        launch(args);
    }
}
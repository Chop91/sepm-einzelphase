package sepm.ws16.e1226745.gui;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.converter.DoubleStringConverter;
import javafx.util.converter.IntegerStringConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sepm.ws16.e1226745.entities.persistence.Jockey;
import sepm.ws16.e1226745.entities.search.JockeySearch;
import sepm.ws16.e1226745.exceptions.ServiceException;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

/**
 * Created by Chop91 on 30.10.2016.
 */
public class JockeysTabViewController {

    private static final Logger logger = LoggerFactory.getLogger(JockeysTabViewController.class);

    @FXML
    private MainViewController mainViewController;

    private JockeyCreateUpdateViewController jockeyCreateUpdateViewController;

    private Stage stage;

    @FXML
    private TableView<Jockey> tableViewJockey;

    @FXML
    private ImageView imageViewJockey;

    @FXML
    private TextField textFieldJockeyFirstName;

    @FXML
    private TextField textFieldJockeyLastName;

    @FXML
    private TextField textFieldJockeyAgeMin;

    @FXML
    private TextField textFieldJockeyAgeMax;

    @FXML
    private TextField textFieldJockeySkillMin;

    @FXML
    private TextField textFieldJockeySkillMax;

    @FXML
    private TableColumn<Jockey, String> tableColumnJockeyId;

    @FXML
    private TableColumn<Jockey, String> tableColumnJockeyFirstName;

    @FXML
    private TableColumn<Jockey, String> tableColumnJockeyLastName;

    @FXML
    private TableColumn<Jockey, String> tableColumnJockeyAge;

    @FXML
    private TableColumn<Jockey, String> tableColumnJockeySkill;

    @FXML
    void buttonJockeySearch_OnAction(ActionEvent event) {
        logger.debug("buttonJockeySearch_OnAction");
        try {
            String firstName = textFieldJockeyFirstName.getText() == null || textFieldJockeyFirstName.getText().isEmpty() ? null : textFieldJockeyFirstName.getText();
            String lastName = textFieldJockeyLastName.getText() == null || textFieldJockeyLastName.getText().isEmpty() ? null : textFieldJockeyLastName.getText();
            Integer lowerAge = textFieldJockeyAgeMin.getText() == null || textFieldJockeyAgeMin.getText().isEmpty() ? null : Integer.valueOf(textFieldJockeyAgeMin.getText());
            Integer upperAge = textFieldJockeyAgeMax.getText() == null || textFieldJockeyAgeMax.getText().isEmpty() ? null : Integer.valueOf(textFieldJockeyAgeMax.getText());
            Double lowerSkill = textFieldJockeySkillMin.getText() == null || textFieldJockeySkillMin.getText().isEmpty() ? null : Double.valueOf(textFieldJockeySkillMin.getText());
            Double upperSkill = textFieldJockeySkillMax.getText() == null || textFieldJockeySkillMax.getText().isEmpty() ? null : Double.valueOf(textFieldJockeySkillMax.getText());

            JockeySearch jockeySearch = new JockeySearch(null, firstName, lastName, lowerAge, upperAge, lowerSkill, upperSkill, false);
            List<Jockey> jockeys = mainViewController.getService().readJockeys(jockeySearch);
            tableViewJockey.setItems(FXCollections.observableArrayList(jockeys));
            tableViewJockey.getSelectionModel().selectFirst();

            if (jockeys.isEmpty()) {
                Alert alert = new CustomAlert(Alert.AlertType.WARNING, "Search Failed", "Searching the jockey failed, see details.", "No entries found.");
                alert.showAndWait();
            }
        } catch (ServiceException e) {
            Alert alert = new CustomAlert(Alert.AlertType.WARNING, "Search Failed", "Searching the jockey failed, see details.", e.getMessage());
            alert.showAndWait();
        }
    }

    @FXML
    void contextMenuMenuItemJockeyAdd_OnAction(ActionEvent event) {
        logger.debug("contextMenuMenuItemJockeyAdd_OnAction");
        try {
            openJockeyCreateUpdateView(null);
            Jockey jockey = jockeyCreateUpdateViewController.getJockeyNew();

            if (jockey != null) {
                mainViewController.getService().createJockey(jockey);
                List<Jockey> jockeys = mainViewController.getService().readAllUndeletedJockeys();
                tableViewJockey.setItems(FXCollections.observableArrayList(jockeys));
                tableViewJockey.getSelectionModel().selectLast();
            }
        } catch (ServiceException e) {
            Alert alert = new CustomAlert(Alert.AlertType.WARNING, "Adding Failed", "Adding the jockey failed, see details.", e.getMessage());
            alert.showAndWait();
        }
    }

    @FXML
    void contextMenuMenuItemJockeyEdit_OnAction(ActionEvent event) {
        logger.debug("contextMenuMenuItemJockeyEdit_OnAction");
        try {
            Jockey jockeyOld = tableViewJockey.getSelectionModel().getSelectedItem();
            openJockeyCreateUpdateView(jockeyOld);
            Jockey jockeyNew = jockeyCreateUpdateViewController.getJockeyNew();

            if (jockeyNew != null) {
                mainViewController.getService().updateJockey(jockeyNew);
                List<Jockey> jockeys = mainViewController.getService().readAllUndeletedJockeys();
                tableViewJockey.setItems(FXCollections.observableArrayList(jockeys));
                tableViewJockey.refresh();
            }
        } catch (ServiceException e) {
            Alert alert = new CustomAlert(Alert.AlertType.WARNING, "Editing Failed", "Editing the jockey failed, see details.", e.getMessage());
            alert.showAndWait();
        }
    }

    @FXML
    void contextMenuMenuItemJockeyDelete_OnAction(ActionEvent event) {
        logger.debug("contextMenuMenuItemJockeyDelete_OnAction");
        try {
            Jockey jockey = tableViewJockey.getSelectionModel().getSelectedItem();

            if (jockey != null) {
                Alert alert = new CustomAlert(Alert.AlertType.CONFIRMATION, "Delete Jockey", "Are you sure you want to delete this Jockey?", String.format("Entry to delete: Jockey with id '%d'", jockey.getId()));
                Optional<ButtonType> result = alert.showAndWait();

                if (result.get() == ButtonType.OK) {
                    mainViewController.getService().deleteJockey(jockey);
                    List<Jockey> jockeys = mainViewController.getService().readAllUndeletedJockeys();
                    tableViewJockey.setItems(FXCollections.observableArrayList(jockeys));
                    imageViewJockey.setImage(null);
                }
            }
        } catch (ServiceException e) {
            Alert alert = new CustomAlert(Alert.AlertType.WARNING, "Deleting Failed", "Deleting the jockey failed, see details.", e.getMessage());
            alert.showAndWait();
        }
    }

    /**
     * Refreshes the table view.
     */
    public void refreshTableView() {
        logger.debug("refreshTableView");
        try {
            List<Jockey> jockeys = mainViewController.getService().readAllUndeletedJockeys();
            tableViewJockey.setItems(FXCollections.observableArrayList(jockeys));
            tableViewJockey.getSelectionModel().selectFirst();

            // FIX: There seems to be a bug with the scrollbar
            // https://stackoverflow.com/questions/37423748/javafx-tablecolumns-headers-not-aligned-with-cells-due-to-vertical-scrollbar
            Platform.runLater(() -> tableViewJockey.refresh());
        } catch (ServiceException e) {
            Alert alert = new CustomAlert(Alert.AlertType.WARNING, "Refreshing Table Failed", "Refreshing the table failed, see details.", e.getMessage());
            alert.showAndWait();
        }
    }

    /**
     * Initializes the controller.
     *
     * @param mainViewController The parent controller.
     */
    public void initialize(MainViewController mainViewController) {
        logger.debug("initialize");
        this.mainViewController = mainViewController;

        textFieldJockeyAgeMin.setTextFormatter(new TextFormatter<>(new IntegerStringConverter()));
        textFieldJockeyAgeMax.setTextFormatter(new TextFormatter<>(new IntegerStringConverter()));
        textFieldJockeySkillMin.setTextFormatter(new TextFormatter<>(new DoubleStringConverter()));
        textFieldJockeySkillMax.setTextFormatter(new TextFormatter<>(new DoubleStringConverter()));

        Tooltip tooltipString = new Tooltip("Enter a string (e.g. 'Rainbow Dash').");
        Tooltip tooltipAge = new Tooltip("Enter a positive integer (e.g. '42').");
        Tooltip tooltipSkill = new Tooltip("Enter a decimal number (e.g. '-4.2').");
        textFieldJockeyFirstName.setTooltip(tooltipString);
        textFieldJockeyLastName.setTooltip(tooltipString);
        textFieldJockeyAgeMin.setTooltip(tooltipAge);
        textFieldJockeyAgeMax.setTooltip(tooltipAge);
        textFieldJockeySkillMin.setTooltip(tooltipSkill);
        textFieldJockeySkillMax.setTooltip(tooltipSkill);

        tableColumnJockeyId.setCellValueFactory(new PropertyValueFactory<>("id"));
        tableColumnJockeyFirstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        tableColumnJockeyLastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        tableColumnJockeyAge.setCellValueFactory(new PropertyValueFactory<>("age"));
        tableColumnJockeySkill.setCellValueFactory(param -> Bindings.format(Locale.US, "%.2f", param.getValue().getSkill()));

        tableViewJockey.getSelectionModel().selectedItemProperty().addListener((observable, oldSelection, newSelection) -> {
            if (newSelection != null) {
                File file = new File(newSelection.getImagePath());
                Image value = new Image(file.toURI().toString());
                imageViewJockey.setImage(value);
            }
        });
    }

    /**
     * Opens the jockey form.
     *
     * @param jockey The jockey to display.
     */
    private void openJockeyCreateUpdateView(Jockey jockey) {
        logger.debug("openJockeyCreateUpdateView");
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("JockeyCreateUpdateView.fxml"));
            AnchorPane anchorPane = fxmlLoader.load();
            stage = new Stage();
            stage.setTitle("Jockey Form");
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(mainViewController.getPrimaryStage());
            stage.setScene(new Scene(anchorPane));

            jockeyCreateUpdateViewController = fxmlLoader.getController();
            jockeyCreateUpdateViewController.setJockey(jockey);
            jockeyCreateUpdateViewController.initialize(this);

            stage.showAndWait();
        } catch (IOException e) {
            logger.error(e.getMessage());
            Alert alert = new CustomAlert(Alert.AlertType.ERROR, "Error", "Opening the jockey form failed, see details.", e.getMessage());
            alert.showAndWait();
        }
    }

    public Stage getStage() {
        return stage;
    }

    public TableView<Jockey> getTableViewJockey() {
        return tableViewJockey;
    }

    public MainViewController getMainViewController() {
        return mainViewController;
    }
}

package sepm.ws16.e1226745.gui;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sepm.ws16.e1226745.entities.persistence.Horse;
import sepm.ws16.e1226745.entities.persistence.Jockey;
import sepm.ws16.e1226745.exceptions.ServiceException;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

/**
 * Created by Chop91 on 01.11.2016.
 */
public class RaceSimulationCreateViewController {

    private static final Logger logger = LoggerFactory.getLogger(RaceSimulationCreateViewController.class);

    @FXML
    private MainViewController mainViewController;

    @FXML
    private TableView<Horse> tableViewRaceSimulationCreateViewHorse;

    @FXML
    private TableColumn<Horse, String> tableColumnRaceSimulationCreateViewHorseId;

    @FXML
    private TableColumn<Horse, String> tableColumnRaceSimulationCreateViewHorseName;

    @FXML
    private TableColumn<Horse, String> tableColumnRaceSimulationCreateViewHorseAge;

    @FXML
    private TableColumn<Horse, String> tableColumnRaceSimulationCreateViewHorseMinSpeed;

    @FXML
    private TableColumn<Horse, String> tableColumnRaceSimulationCreateViewHorseMaxSpeed;

    @FXML
    private TableView<Jockey> tableViewRaceSimulationCreateViewJockey;

    @FXML
    private TableColumn<Jockey, String> tableColumnRaceSimulationCreateViewJockeyId;

    @FXML
    private TableColumn<Jockey, String> tableColumnRaceSimulationCreateViewJockeyFirstName;

    @FXML
    private TableColumn<Jockey, String> tableColumnRaceSimulationCreateViewJockeyLastName;

    @FXML
    private TableColumn<Jockey, String> tableColumnRaceSimulationCreateViewJockeyAge;

    @FXML
    private TableColumn<Jockey, String> tableColumnRaceSimulationCreateViewJockeySkill;

    @FXML
    private TableView<Map.Entry<Horse, Jockey>> tableViewRaceSimulationCreateViewRaceSimulation;

    @FXML
    private TableColumn<Map.Entry<Horse, Jockey>, String> tableColumnRaceSimulationCreateViewRaceSimulationHorseId;

    @FXML
    private TableColumn<Map.Entry<Horse, Jockey>, String> tableColumnRaceSimulationCreateViewRaceSimulationHorseName;

    @FXML
    private TableColumn<Map.Entry<Horse, Jockey>, String> tableColumnRaceSimulationCreateViewRaceSimulationJockeyId;

    @FXML
    private TableColumn<Map.Entry<Horse, Jockey>, String> tableColumnRaceSimulationCreateViewRaceSimulationJockeyName;

    @FXML
    void contextMenuMenuItemRaceSimulationDelete_OnAction(ActionEvent event) {
        logger.debug("contextMenuMenuItemRaceSimulationDelete_OnAction");

        Map.Entry<Horse, Jockey> competitor = tableViewRaceSimulationCreateViewRaceSimulation.getSelectionModel().getSelectedItem();

        if (competitor != null) {
            Alert alert = new CustomAlert(Alert.AlertType.CONFIRMATION, "Delete Horse-Jockey combination", "Are you sure you want to delete this Horse-Jockey combination?", String.format("Entry to delete: Horse-Jockey combination with Horse ID '%d' and Jockey ID '%d'", competitor.getKey().getId(), competitor.getValue().getId()));
            Optional<ButtonType> result = alert.showAndWait();

            if (result.get() == ButtonType.OK) {
                try {
                    mainViewController.getService().removeHorseJockeyPairFromRaceSimulation(competitor.getKey(), competitor.getValue());
                    Map<Horse, Jockey> currentCompetitors = mainViewController.getService().getCurrentCompetitors();

                    tableViewRaceSimulationCreateViewRaceSimulation.setItems(FXCollections.observableArrayList(currentCompetitors.entrySet()));
                    tableViewRaceSimulationCreateViewRaceSimulation.getSelectionModel().selectLast();

                } catch (ServiceException e) {
                    alert = new CustomAlert(Alert.AlertType.WARNING, "Deleting Horse-Jockey Pair Failed", "Deleting the horse-jockey pair failed, see details.", e.getMessage());
                    alert.showAndWait();
                }
            }
        }
    }

    @FXML
    void buttonRaceSimulationCreateViewAddToRaceSimulation_OnAction(ActionEvent event) {
        logger.debug("buttonRaceSimulationCreateViewAddToRaceSimulation_OnAction");

        try {
            Horse horse = tableViewRaceSimulationCreateViewHorse.getSelectionModel().getSelectedItem();
            Jockey jockey = tableViewRaceSimulationCreateViewJockey.getSelectionModel().getSelectedItem();

            mainViewController.getService().addHorseJockeyPairToRaceSimulation(horse, jockey);
            Map<Horse, Jockey> currentCompetitors = mainViewController.getService().getCurrentCompetitors();

            tableViewRaceSimulationCreateViewRaceSimulation.setItems(FXCollections.observableArrayList(currentCompetitors.entrySet()));
            tableViewRaceSimulationCreateViewRaceSimulation.getSelectionModel().selectLast();
        } catch (ServiceException e) {
            Alert alert = new CustomAlert(Alert.AlertType.WARNING, "Adding Horse-Jockey Pair Failed", "Adding the horse-jockey pair failed, see details.", e.getMessage());
            alert.showAndWait();
        }
    }

    @FXML
    void buttonRaceSimulationCreateViewStart_OnAction(ActionEvent event) {
        logger.debug("buttonRaceSimulationCreateViewStart_OnAction");
        mainViewController.getService().getAllCompetitors().putAll(mainViewController.getService().getCurrentCompetitors());
        mainViewController.getStage().close();
    }

    @FXML
    void buttonRaceSimulationCreateViewCancel_OnAction(ActionEvent event) {
        logger.debug("buttonRaceSimulationCreateViewCancel_OnAction");
        mainViewController.getService().getCurrentCompetitors().clear();
        mainViewController.getStage().close();
    }

    /**
     * Initializes the controller.
     *
     * @param mainViewController The parent controller.
     */
    public void initialize(MainViewController mainViewController) {
        logger.debug("initialize");

        try {
            this.mainViewController = mainViewController;

            tableColumnRaceSimulationCreateViewJockeyId.setCellValueFactory(new PropertyValueFactory<>("id"));
            tableColumnRaceSimulationCreateViewJockeyFirstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
            tableColumnRaceSimulationCreateViewJockeyLastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
            tableColumnRaceSimulationCreateViewJockeyAge.setCellValueFactory(new PropertyValueFactory<>("age"));
            tableColumnRaceSimulationCreateViewJockeySkill.setCellValueFactory(param -> Bindings.format(Locale.US, "%.2f", param.getValue().getSkill()));

            List<Jockey> jockeys = mainViewController.getService().readAllUndeletedJockeys();
            tableViewRaceSimulationCreateViewJockey.setItems(FXCollections.observableArrayList(jockeys));
            tableViewRaceSimulationCreateViewJockey.getSelectionModel().selectFirst();

            tableColumnRaceSimulationCreateViewHorseId.setCellValueFactory(new PropertyValueFactory<>("id"));
            tableColumnRaceSimulationCreateViewHorseName.setCellValueFactory(new PropertyValueFactory<>("name"));
            tableColumnRaceSimulationCreateViewHorseAge.setCellValueFactory(new PropertyValueFactory<>("age"));
            tableColumnRaceSimulationCreateViewHorseMinSpeed.setCellValueFactory(param -> Bindings.format(Locale.US, "%.2f", param.getValue().getMinSpeed()));
            tableColumnRaceSimulationCreateViewHorseMaxSpeed.setCellValueFactory(param -> Bindings.format(Locale.US, "%.2f", param.getValue().getMaxSpeed()));

            List<Horse> horses = mainViewController.getService().readAllUndeletedHorses();
            tableViewRaceSimulationCreateViewHorse.setItems(FXCollections.observableArrayList(horses));
            tableViewRaceSimulationCreateViewHorse.getSelectionModel().selectFirst();

            tableColumnRaceSimulationCreateViewRaceSimulationHorseId.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getKey().getId().toString()));
            tableColumnRaceSimulationCreateViewRaceSimulationHorseName.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getKey().getName()));
            tableColumnRaceSimulationCreateViewRaceSimulationJockeyId.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getValue().getId().toString()));
            tableColumnRaceSimulationCreateViewRaceSimulationJockeyName.setCellValueFactory(param -> new SimpleStringProperty(String.format("%s %s", param.getValue().getValue().getFirstName(), param.getValue().getValue().getLastName())));

            // FIX: There seems to be a bug with the scrollbar
            // https://stackoverflow.com/questions/37423748/javafx-tablecolumns-headers-not-aligned-with-cells-due-to-vertical-scrollbar
            Platform.runLater(() -> tableViewRaceSimulationCreateViewJockey.refresh());
            Platform.runLater(() -> tableViewRaceSimulationCreateViewHorse.refresh());
            Platform.runLater(() -> tableViewRaceSimulationCreateViewRaceSimulation.refresh());
        } catch (ServiceException e) {
            Alert alert = new CustomAlert(Alert.AlertType.WARNING, "Initializing Race Simulation Form Failed", "Initializing the race simulation form failed, see details.", e.getMessage());
            alert.showAndWait();
        }
    }
}
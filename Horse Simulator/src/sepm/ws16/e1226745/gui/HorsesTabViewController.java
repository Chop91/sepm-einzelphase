package sepm.ws16.e1226745.gui;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.converter.DoubleStringConverter;
import javafx.util.converter.IntegerStringConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sepm.ws16.e1226745.entities.persistence.Horse;
import sepm.ws16.e1226745.entities.search.HorseSearch;
import sepm.ws16.e1226745.exceptions.ServiceException;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

/**
 * Created by Chop91 on 30.10.2016.
 */
public class HorsesTabViewController {

    private static final Logger logger = LoggerFactory.getLogger(HorsesTabViewController.class);

    @FXML
    private MainViewController mainViewController;

    private HorseCreateUpdateViewController horseCreateUpdateViewController;

    private Stage stage;

    @FXML
    private TableView<Horse> tableViewHorse;

    @FXML
    private ImageView imageViewHorse;

    @FXML
    private TextField textFieldHorseName;

    @FXML
    private TextField textFieldHorseAgeMin;

    @FXML
    private TextField textFieldHorseAgeMax;

    @FXML
    private TextField textFieldHorseMinSpeedMin;

    @FXML
    private TextField textFieldHorseMinSpeedMax;

    @FXML
    private TextField textFieldHorseMaxSpeedMin;

    @FXML
    private TextField textFieldHorseMaxSpeedMax;

    @FXML
    private TableColumn<Horse, String> tableColumnHorseId;

    @FXML
    private TableColumn<Horse, String> tableColumnHorseName;

    @FXML
    private TableColumn<Horse, String> tableColumnHorseAge;

    @FXML
    private TableColumn<Horse, String> tableColumnHorseMinSpeed;

    @FXML
    private TableColumn<Horse, String> tableColumnHorseMaxSpeed;

    @FXML
    void buttonHorseSearch_OnAction(ActionEvent event) {
        logger.debug("buttonHorseSearch_OnAction");
        try {
            String name = textFieldHorseName.getText() == null || textFieldHorseName.getText().isEmpty() ? null : textFieldHorseName.getText();
            Integer lowerAge = textFieldHorseAgeMin.getText() == null || textFieldHorseAgeMin.getText().isEmpty() ? null : Integer.valueOf(textFieldHorseAgeMin.getText());
            Integer upperAge = textFieldHorseAgeMax.getText() == null || textFieldHorseAgeMax.getText().isEmpty() ? null : Integer.valueOf(textFieldHorseAgeMax.getText());
            Double lowerMinSpeed = textFieldHorseMinSpeedMin.getText() == null || textFieldHorseMinSpeedMin.getText().isEmpty() ? null : Double.valueOf(textFieldHorseMinSpeedMin.getText());
            Double upperMinSpeed = textFieldHorseMinSpeedMax.getText() == null || textFieldHorseMinSpeedMax.getText().isEmpty() ? null : Double.valueOf(textFieldHorseMinSpeedMax.getText());
            Double lowerMaxSpeed = textFieldHorseMaxSpeedMin.getText() == null || textFieldHorseMaxSpeedMin.getText().isEmpty() ? null : Double.valueOf(textFieldHorseMaxSpeedMin.getText());
            Double upperMaxSpeed = textFieldHorseMaxSpeedMax.getText() == null || textFieldHorseMaxSpeedMax.getText().isEmpty() ? null : Double.valueOf(textFieldHorseMaxSpeedMax.getText());

            HorseSearch horseSearch = new HorseSearch(null, name, lowerAge, upperAge, lowerMinSpeed, upperMinSpeed, lowerMaxSpeed, upperMaxSpeed, false);
            List<Horse> horses = mainViewController.getService().readHorses(horseSearch);
            tableViewHorse.setItems(FXCollections.observableArrayList(horses));
            tableViewHorse.getSelectionModel().selectFirst();

            if (horses.isEmpty()) {
                Alert alert = new CustomAlert(Alert.AlertType.WARNING, "Search Failed", "Searching the horse failed, see details.", "No entries found.");
                alert.showAndWait();
            }
        } catch (ServiceException e) {
            Alert alert = new CustomAlert(Alert.AlertType.WARNING, "Search Failed", "Searching the horse failed, see details.", e.getMessage());
            alert.showAndWait();
        }
    }

    @FXML
    void contextMenuMenuItemHorseAdd_OnAction(ActionEvent event) {
        logger.debug("contextMenuMenuItemHorseAdd_OnAction");
        try {
            openHorseCreateUpdateView(null);
            Horse horse = horseCreateUpdateViewController.getHorseNew();

            if (horse != null) {
                mainViewController.getService().createHorse(horse);
                List<Horse> horses = mainViewController.getService().readAllUndeletedHorses();
                tableViewHorse.setItems(FXCollections.observableArrayList(horses));
                tableViewHorse.getSelectionModel().selectLast();
            }
        } catch (ServiceException e) {
            Alert alert = new CustomAlert(Alert.AlertType.WARNING, "Adding Failed", "Adding the horse failed, see details.", e.getMessage());
            alert.showAndWait();
        }
    }

    @FXML
    void contextMenuMenuItemHorseEdit_OnAction(ActionEvent event) {
        logger.debug("contextMenuMenuItemHorseEdit_OnAction");
        try {
            Horse horseOld = tableViewHorse.getSelectionModel().getSelectedItem();
            openHorseCreateUpdateView(horseOld);
            Horse horseNew = horseCreateUpdateViewController.getHorseNew();

            if (horseNew != null) {
                mainViewController.getService().updateHorse(horseNew);
                List<Horse> horses = mainViewController.getService().readAllUndeletedHorses();
                tableViewHorse.setItems(FXCollections.observableArrayList(horses));
                tableViewHorse.refresh();
            }
        } catch (ServiceException e) {
            Alert alert = new CustomAlert(Alert.AlertType.WARNING, "Editing Failed", "Editing the horse failed, see details.", e.getMessage());
            alert.showAndWait();
        }
    }

    @FXML
    void contextMenuMenuItemHorseDelete_OnAction(ActionEvent event) {
        logger.debug("contextMenuMenuItemHorseDelete_OnAction");
        try {
            Horse horse = tableViewHorse.getSelectionModel().getSelectedItem();

            if (horse != null) {
                Alert alert = new CustomAlert(Alert.AlertType.CONFIRMATION, "Delete Horse", "Are you sure you want to delete this Horse?", String.format("Entry to delete: Horse with id '%d'", horse.getId()));
                Optional<ButtonType> result = alert.showAndWait();

                if (result.get() == ButtonType.OK) {
                    mainViewController.getService().deleteHorse(horse);
                    List<Horse> horses = mainViewController.getService().readAllUndeletedHorses();
                    tableViewHorse.setItems(FXCollections.observableArrayList(horses));
                    imageViewHorse.setImage(null);
                }
            }
        } catch (ServiceException e) {
            Alert alert = new CustomAlert(Alert.AlertType.WARNING, "Deleting Failed", "Deleting the horse failed, see details.", e.getMessage());
            alert.showAndWait();
        }
    }

    /**
     * Refreshes the table view.
     */
    public void refreshTableView() {
        logger.debug("refreshTableView");
        try {
            List<Horse> horses = mainViewController.getService().readAllUndeletedHorses();
            tableViewHorse.setItems(FXCollections.observableArrayList(horses));
            tableViewHorse.getSelectionModel().selectFirst();

            // FIX: There seems to be a bug with the scrollbar
            // https://stackoverflow.com/questions/37423748/javafx-tablecolumns-headers-not-aligned-with-cells-due-to-vertical-scrollbar
            Platform.runLater(() -> tableViewHorse.refresh());
        } catch (ServiceException e) {
            Alert alert = new CustomAlert(Alert.AlertType.WARNING, "Refreshing Table Failed", "Refreshing the table failed, see details.", e.getMessage());
            alert.showAndWait();
        }
    }

    /**
     * Initializes the controller.
     *
     * @param mainViewController The parent controller.
     */
    public void initialize(MainViewController mainViewController) {
        logger.debug("initialize");
        this.mainViewController = mainViewController;

        textFieldHorseAgeMin.setTextFormatter(new TextFormatter<>(new IntegerStringConverter()));
        textFieldHorseAgeMax.setTextFormatter(new TextFormatter<>(new IntegerStringConverter()));
        textFieldHorseMinSpeedMin.setTextFormatter(new TextFormatter<>(new DoubleStringConverter()));
        textFieldHorseMinSpeedMax.setTextFormatter(new TextFormatter<>(new DoubleStringConverter()));
        textFieldHorseMaxSpeedMin.setTextFormatter(new TextFormatter<>(new DoubleStringConverter()));
        textFieldHorseMaxSpeedMax.setTextFormatter(new TextFormatter<>(new DoubleStringConverter()));

        Tooltip tooltipString = new Tooltip("Enter a string (e.g. 'Rainbow Dash').");
        Tooltip tooltipAge = new Tooltip("Enter a positive integer (e.g. '42').");
        Tooltip tooltipSpeed = new Tooltip("Enter a decimal number (e.g. '-4.2').");
        textFieldHorseName.setTooltip(tooltipString);
        textFieldHorseAgeMin.setTooltip(tooltipAge);
        textFieldHorseAgeMax.setTooltip(tooltipAge);
        textFieldHorseMinSpeedMin.setTooltip(tooltipSpeed);
        textFieldHorseMinSpeedMax.setTooltip(tooltipSpeed);
        textFieldHorseMaxSpeedMin.setTooltip(tooltipSpeed);
        textFieldHorseMaxSpeedMax.setTooltip(tooltipSpeed);

        tableColumnHorseId.setCellValueFactory(new PropertyValueFactory<>("id"));
        tableColumnHorseName.setCellValueFactory(new PropertyValueFactory<>("name"));
        tableColumnHorseAge.setCellValueFactory(new PropertyValueFactory<>("age"));
        tableColumnHorseMinSpeed.setCellValueFactory(param -> Bindings.format(Locale.US, "%.2f", param.getValue().getMinSpeed()));
        tableColumnHorseMaxSpeed.setCellValueFactory(param -> Bindings.format(Locale.US, "%.2f", param.getValue().getMaxSpeed()));

        tableViewHorse.getSelectionModel().selectedItemProperty().addListener((observable, oldSelection, newSelection) -> {
            if (newSelection != null) {
                File file = new File(newSelection.getImagePath());
                Image value = new Image(file.toURI().toString());
                imageViewHorse.setImage(value);
            }
        });
    }

    /**
     * Opens the horse form.
     *
     * @param horse The horse to display.
     */
    private void openHorseCreateUpdateView(Horse horse) {
        logger.debug("openHorseCreateUpdateView");
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("HorseCreateUpdateView.fxml"));
            AnchorPane anchorPane = fxmlLoader.load();
            stage = new Stage();
            stage.setTitle("Horse Form");
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(mainViewController.getPrimaryStage());
            stage.setScene(new Scene(anchorPane));

            horseCreateUpdateViewController = fxmlLoader.getController();
            horseCreateUpdateViewController.setHorse(horse);
            horseCreateUpdateViewController.initialize(this);

            stage.showAndWait();
        } catch (IOException e) {
            logger.error(e.getMessage());
            Alert alert = new CustomAlert(Alert.AlertType.ERROR, "Error", "Opening the horse form failed, see details.", e.getMessage());
            alert.showAndWait();
        }
    }

    public Stage getStage() {
        return stage;
    }

    public TableView<Horse> getTableViewHorse() {
        return tableViewHorse;
    }

    public MainViewController getMainViewController() {
        return mainViewController;
    }
}
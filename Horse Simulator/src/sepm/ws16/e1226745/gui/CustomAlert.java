package sepm.ws16.e1226745.gui;

import javafx.beans.NamedArg;
import javafx.scene.control.Alert;

/**
 * Created by Chop91 on 01.11.2016.
 */
public class CustomAlert extends Alert {

    public CustomAlert(@NamedArg("alertType") AlertType alertType, @NamedArg("title") String title, @NamedArg("headerText") String headerText, @NamedArg("contentText") String contentText) {
        super(alertType);
        setTitle(title);
        setHeaderText(headerText);
        setContentText(contentText);
    }
}

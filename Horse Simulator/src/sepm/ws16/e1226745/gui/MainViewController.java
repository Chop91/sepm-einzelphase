package sepm.ws16.e1226745.gui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sepm.ws16.e1226745.exceptions.ServiceException;
import sepm.ws16.e1226745.service.Service;
import sepm.ws16.e1226745.service.SimpleService;

import java.io.File;
import java.io.IOException;

/**
 * Created by Chop91 on 30.10.2016.
 */
public class MainViewController {

    private static final Logger logger = LoggerFactory.getLogger(MainViewController.class);

    private Service service;

    private Stage primaryStage;

    private Stage stage;

    private FileChooser imageFileChooser;

    @FXML
    private RaceSimulationCreateViewController raceSimulationCreateViewController;

    @FXML
    private HorsesTabViewController horsesTabViewController;

    @FXML
    private JockeysTabViewController jockeysTabViewController;

    @FXML
    private RaceSimulationTabViewController raceSimulationTabViewController;

    @FXML
    void menuItemGenerateTestData_OnAction(ActionEvent event) {
        logger.debug("menuItemGenerateTestData_OnAction");
        try {
            service.generateTestData();
            jockeysTabViewController.refreshTableView();
            horsesTabViewController.refreshTableView();
            raceSimulationTabViewController.refreshTableView();
            logger.debug("Test Data generated.");
        } catch (ServiceException e) {
            Alert alert = new CustomAlert(Alert.AlertType.WARNING, "Generating Test Data Failed", "Generating the test data failed, see details.", e.getMessage());
            alert.showAndWait();
        }
    }

    @FXML
    void menuItemNewRaceSimulation_OnAction(ActionEvent event) {
        logger.debug("menuItemNewRaceSimulation_OnAction");
        openRaceSimulationCreateView();
    }

    /**
     * Starts the controller.
     *
     * @param stage The parent stage.
     */
    public void start(Stage stage) {
        logger.debug("start");

        this.primaryStage = stage;
        try {
            service = new SimpleService();

            imageFileChooser = new FileChooser();
            imageFileChooser.setTitle("Select Image...");
            FileChooser.ExtensionFilter extensionFilterPng = new FileChooser.ExtensionFilter("PNG (*.png)", "*.png");
            FileChooser.ExtensionFilter extensionFilterJpg = new FileChooser.ExtensionFilter("JPEG (*.jpg;*.jpeg)", "*.jpg", "*.jpeg");
            FileChooser.ExtensionFilter extensionFilterTiff = new FileChooser.ExtensionFilter("TIFF (*.tif;*.tiff)", "*.tif", "*.tiff");
            FileChooser.ExtensionFilter extensionFilterBmp = new FileChooser.ExtensionFilter("Bitmap (*.bmp)", "*.bmp");
            imageFileChooser.getExtensionFilters().addAll(extensionFilterPng, extensionFilterJpg, extensionFilterTiff, extensionFilterBmp);

            horsesTabViewController.initialize(this);
            jockeysTabViewController.initialize(this);
            raceSimulationTabViewController.initialize(this);
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            Alert alert = new CustomAlert(Alert.AlertType.ERROR, "Error", "Could not start the service, see details.", e.getMessage());
            alert.showAndWait();
            stop();
        }
    }

    /**
     * Stops the controller.
     */
    public void stop() {
        logger.debug("stop");

        try {
            service.closeService();
        } catch (ServiceException e) {
            Alert alert = new CustomAlert(Alert.AlertType.ERROR, "Error", "Could not close service, see details.", e.getMessage());
            alert.showAndWait();
        }
    }

    /**
     * Opens the race simulation form.
     */
    private void openRaceSimulationCreateView() {
        logger.debug("openRaceSimulationCreateView");

        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("RaceSimulationCreateView.fxml"));
            AnchorPane anchorPane = fxmlLoader.load();
            stage = new Stage();
            stage.setTitle("Race Simulation Form");
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(primaryStage);
            stage.setScene(new Scene(anchorPane));
            service.createRaceSimulation();

            raceSimulationCreateViewController = fxmlLoader.getController();
            raceSimulationCreateViewController.initialize(this);

            stage.showAndWait();

            if (!service.getCurrentCompetitors().isEmpty()) {
                service.executeRaceSimulation();
                raceSimulationTabViewController.refreshTableView();
                logger.info("Race Simulation created.");
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
            Alert alert = new CustomAlert(Alert.AlertType.ERROR, "Error", "Opening the race simulation form failed, see details.", e.getMessage());
            alert.showAndWait();
        } catch (ServiceException e) {
            Alert alert = new CustomAlert(Alert.AlertType.WARNING, "Executing Race Simulation Failed", "Executing the race simulation failed, see details.", e.getMessage());
            alert.showAndWait();
        }
    }

    /**
     * Selects an image using a file chooser.
     *
     * @param stage The parent.
     * @return The image path.
     */
    public String selectImageFile(Stage stage) {
        logger.debug("selectImageFile");

        File file = imageFileChooser.showOpenDialog(stage);
        if (file != null) {
            String path = file.getAbsolutePath();
            String imagePath = path == null || path.isEmpty() ? null : path;
            return imagePath;
        }
        return null;
    }

    public Service getService() {
        return service;
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public Stage getStage() {
        return stage;
    }

    public JockeysTabViewController getJockeysTabViewController() {
        return jockeysTabViewController;
    }

    public HorsesTabViewController getHorsesTabViewController() {
        return horsesTabViewController;
    }

}
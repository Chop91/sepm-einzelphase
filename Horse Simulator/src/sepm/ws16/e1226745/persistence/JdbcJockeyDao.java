package sepm.ws16.e1226745.persistence;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sepm.ws16.e1226745.entities.persistence.Jockey;
import sepm.ws16.e1226745.entities.search.JockeySearch;
import sepm.ws16.e1226745.exceptions.DaoException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by chop91 on 27.10.16.
 */
public class JdbcJockeyDao implements JockeyDao {

    private static final Logger logger = LoggerFactory.getLogger(JdbcJockeyDao.class);

    private Connection connection;

    private static final String CREATE_STATEMENT = "INSERT INTO Jockey(FirstName, LastName, Age, Skill, ImagePath) VALUES (?, ?, ?, ?, ?);";
    private static final String UPDATE_STATEMENT = "UPDATE Jockey SET FirstName = ?, LastName = ?, Age = ?, Skill = ?, ImagePath = ?, IsDeleted = ? WHERE Id = ?;";

    public JdbcJockeyDao(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Jockey create(Jockey jockey) throws DaoException {
        logger.debug("create '{}'", jockey);

        checkIfJockeyIsNull(jockey);

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(CREATE_STATEMENT, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, jockey.getFirstName());
            preparedStatement.setString(2, jockey.getLastName());
            preparedStatement.setInt(3, jockey.getAge());
            preparedStatement.setDouble(4, jockey.getSkill());
            preparedStatement.setString(5, jockey.getImagePath());
            preparedStatement.executeUpdate();

            // Set the provided id in the entity and update it
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            resultSet.next();
            jockey.setId(resultSet.getInt(1));
            jockey.setDeleted(false);
            update(jockey);
            logger.debug("created '{}'", jockey);
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e.getMessage());
        }
        return jockey;
    }

    @Override
    public List<Jockey> read(JockeySearch jockeySearch) throws DaoException {
        logger.debug("read '{}'", jockeySearch);

        checkIfJockeySearchIsNull(jockeySearch);

        // Create a dynamic query for the provided parameters
        DynamicQueryGenerator.generateQuery(jockeySearch);
        String statement = DynamicQueryGenerator.getGeneratedQuery();
        Map<Integer, Object> map = DynamicQueryGenerator.getGeneratedMapping();

        ArrayList<Jockey> jockeys = new ArrayList();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(statement);
            for (Map.Entry<Integer, Object> entry : map.entrySet()) {
                preparedStatement.setObject(entry.getKey(), entry.getValue());
            }

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                jockeys.add(new Jockey(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getInt(4), resultSet.getDouble(5), resultSet.getString(6), resultSet.getBoolean(7)));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e.getMessage());
        }

        return jockeys;
    }

    @Override
    public List<Jockey> readAll() throws DaoException {
        logger.debug("readAll");
        return read(new JockeySearch(null, true));
    }

    @Override
    public List<Jockey> readAllUndeleted() throws DaoException {
        logger.debug("readAllUndeleted");
        return read(new JockeySearch(null, false));
    }

    @Override
    public void update(Jockey jockey) throws DaoException {
        logger.debug("update '{}'", jockey);

        checkIfJockeyExists(jockey);

        try {
            // Update jockey
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_STATEMENT);
            preparedStatement.setString(1, jockey.getFirstName());
            preparedStatement.setString(2, jockey.getLastName());
            preparedStatement.setInt(3, jockey.getAge());
            preparedStatement.setDouble(4, jockey.getSkill());
            preparedStatement.setString(5, jockey.getImagePath());
            preparedStatement.setBoolean(6, jockey.getDeleted());
            preparedStatement.setInt(7, jockey.getId());
            preparedStatement.executeUpdate();

            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                logger.error(e.getMessage());
                throw new DaoException(e.getMessage());
            }
        }
    }

    @Override
    public void delete(Jockey jockey) throws DaoException {
        logger.debug("delete '{}'", jockey);

        checkIfJockeyExists(jockey);
        jockey.setDeleted(true);
        update(jockey);
    }

    /**
     * Checks if the jockey search is null.
     *
     * @param jockeySearch The search parameter.
     * @throws DaoException If the jockey search is null.
     */
    private void checkIfJockeySearchIsNull(JockeySearch jockeySearch) throws DaoException {
        logger.debug("checkIfJockeySearchIsNull '{}'", jockeySearch);

        if (jockeySearch == null) {
            logAndThrowDaoException(String.format("'JockeySearch' is null."));
        }
    }

    /**
     * Checks if the jockey is null.
     *
     * @param jockey The jockey.
     * @throws DaoException If the jockey is null.
     */
    private void checkIfJockeyIsNull(Jockey jockey) throws DaoException {
        logger.debug("checkIfJockeyIsNull '{}'", jockey);

        if (jockey == null) {
            logAndThrowDaoException(String.format("'Jockey' is null."));
        }
    }

    /**
     * Checks if the jockey exists.
     *
     * @param jockey The jockey.
     * @throws DaoException If the jockey does not exist.
     */
    private void checkIfJockeyExists(Jockey jockey) throws DaoException {
        logger.debug("checkIfJockeyExists '{}'", jockey);

        checkIfJockeyIsNull(jockey);

        JockeySearch jockeySearch = new JockeySearch(jockey.getId(), false);
        List<Jockey> jockeys = read(jockeySearch);
        if (jockeys.isEmpty()) {
            logAndThrowDaoException(String.format("'Jockey' %s does not exist.", jockey));
        }
    }

    /**
     * Logs and throws a dao exception.
     *
     * @param message The message to log and throw.
     * @throws DaoException Always, because it is the purpose of this method.
     */
    private static void logAndThrowDaoException(String message) throws DaoException {
        logger.warn(message);
        throw new DaoException(message);
    }
}
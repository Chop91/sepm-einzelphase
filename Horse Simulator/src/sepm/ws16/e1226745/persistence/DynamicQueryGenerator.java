package sepm.ws16.e1226745.persistence;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sepm.ws16.e1226745.entities.search.HorseSearch;
import sepm.ws16.e1226745.entities.search.JockeySearch;
import sepm.ws16.e1226745.entities.search.RaceSimulationSearch;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Chop91 on 01.11.2016.
 */
public class DynamicQueryGenerator {

    private static final Logger logger = LoggerFactory.getLogger(DynamicQueryGenerator.class);

    private static final String READ_STATEMENT_HORSE = "SELECT * FROM Horse WHERE 1 = 1";
    private static final String READ_STATEMENT_JOCKEY = "SELECT * FROM Jockey WHERE 1 = 1";
    private static final String READ_STATEMENT_RACE_SIMULATION = "SELECT * FROM RaceSimulation WHERE 1 = 1";

    private static String generatedQuery;
    private static Map<Integer, Object> generatedMapping;
    private static Integer index;

    /**
     * Dynamically generates a query for a horse search.
     *
     * @param horseSearch The search parameters.
     */
    public static void generateQuery(HorseSearch horseSearch) {
        logger.debug("generateQuery '{}'", horseSearch);

        generatedQuery = new String(READ_STATEMENT_HORSE);
        index = 1;
        generatedMapping = new HashMap<>();
        generateQueryPartInteger(horseSearch.getId(), "Id");
        generateQueryPartStringLike(horseSearch.getName(), "Name");
        generateQueryPartIntegerRange(horseSearch.getLowerAge(), horseSearch.getUpperAge(), "Age");
        generateQueryPartDoubleRange(horseSearch.getLowerMinSpeed(), horseSearch.getUpperMinSpeed(), "MinSpeed");
        generateQueryPartDoubleRange(horseSearch.getLowerMaxSpeed(), horseSearch.getUpperMaxSpeed(), "MaxSpeed");
        generateQueryPartBoolean(horseSearch.getHorseIsDeleted(), "IsDeleted");
        generatedQuery += ";";
        logger.debug("generatedQuery '{}'", generatedQuery);
    }

    /**
     * Dynamically generates a query for a jockey search.
     *
     * @param jockeySearch The search parameters.
     */
    public static void generateQuery(JockeySearch jockeySearch) {
        logger.debug("generateQuery '{}'", jockeySearch);

        generatedQuery = new String(READ_STATEMENT_JOCKEY);
        index = 1;
        generatedMapping = new HashMap<>();
        generateQueryPartInteger(jockeySearch.getId(), "Id");
        generateQueryPartStringLike(jockeySearch.getFirstName(), "FirstName");
        generateQueryPartStringLike(jockeySearch.getLastName(), "LastName");
        generateQueryPartIntegerRange(jockeySearch.getLowerAge(), jockeySearch.getUpperAge(), "Age");
        generateQueryPartDoubleRange(jockeySearch.getLowerSkill(), jockeySearch.getUpperSkill(), "Skill");
        generateQueryPartBoolean(jockeySearch.getJockeyIsDeleted(), "IsDeleted");
        generatedQuery += ";";
        logger.debug("generatedQuery '{}'", generatedQuery);
    }

    /**
     * Dynamically generates a query for a race simulation search.
     *
     * @param raceSimulationSearch The search parameters.
     */
    public static void generateQuery(RaceSimulationSearch raceSimulationSearch) {
        logger.debug("generateQuery '{}'", raceSimulationSearch);

        generatedQuery = new String(READ_STATEMENT_RACE_SIMULATION);
        index = 1;
        generatedMapping = new HashMap<>();
        generateQueryPartInteger(raceSimulationSearch.getId(), "Id");
        generateQueryPartInteger(raceSimulationSearch.getHorseId(), "HorseId");
        generateQueryPartInteger(raceSimulationSearch.getJockeyId(), "JockeyId");
        generatedQuery += ";";
        logger.debug("generatedQuery '{}'", generatedQuery);
    }

    /**
     * Dynamically generates a query part for an integer.
     *
     * @param value     The value to set.
     * @param attribute The attribute name.
     */
    private static void generateQueryPartInteger(Integer value, String attribute) {
        logger.debug("generateQueryPartInteger '({}, {})'", value, attribute);

        if (value != null) {
            generatedQuery += String.format(" AND %s = ?", attribute);
            generatedMapping.put(index, value);
            index++;
        }
    }

    /**
     * Dynamically generates a query part for an integer range.
     *
     * @param from      The from value to set.
     * @param to        The to value to set.
     * @param attribute The attribute name.
     */
    private static void generateQueryPartIntegerRange(Integer from, Integer to, String attribute) {
        logger.debug("generateQueryPartIntegerRange '({}, {}, {})'", from, to, attribute);

        if (from != null && to != null) {
            generatedQuery += String.format(" AND %s BETWEEN ? AND ?", attribute);
            generatedMapping.put(index, from);
            index++;
            generatedMapping.put(index, to);
            index++;
        } else if (from == null && to != null) {
            generatedQuery += String.format(" AND %s BETWEEN ? AND ?", attribute);
            generatedMapping.put(index, Integer.MIN_VALUE);
            index++;
            generatedMapping.put(index, to);
            index++;
        } else if (from != null && to == null) {
            generatedQuery += String.format(" AND %s BETWEEN ? AND ?", attribute);
            generatedMapping.put(index, from);
            index++;
            generatedMapping.put(index, Integer.MAX_VALUE);
            index++;
        }

    }

    /**
     * Dynamically generates a query part for a double range.
     *
     * @param from      The from value to set.
     * @param to        The to value to set.
     * @param attribute The attribute name.
     */
    private static void generateQueryPartDoubleRange(Double from, Double to, String attribute) {
        logger.debug("generateQueryPartDoubleRange '({}, {}, {})'", from, to, attribute);

        if (from != null && to != null) {
            generatedQuery += String.format(" AND %s BETWEEN ? AND ?", attribute);
            generatedMapping.put(index, from);
            index++;
            generatedMapping.put(index, to);
            index++;
        } else if (from == null && to != null) {
            generatedQuery += String.format(" AND %s BETWEEN ? AND ?", attribute);
            generatedMapping.put(index, Double.MIN_VALUE);
            index++;
            generatedMapping.put(index, to);
            index++;
        } else if (from != null && to == null) {
            generatedQuery += String.format(" AND %s BETWEEN ? AND ?", attribute);
            generatedMapping.put(index, from);
            index++;
            generatedMapping.put(index, Double.MAX_VALUE);
            index++;
        }
    }

    /**
     * Dynamically generates a query part for a string using SQL LIKE.
     *
     * @param value     The value to set.
     * @param attribute The attribute name.
     */
    private static void generateQueryPartStringLike(String value, String attribute) {
        logger.debug("generateQueryPartStringLike '({}, {})'", value, attribute);

        if (value != null) {
            generatedQuery += String.format(" AND %s LIKE ?", attribute);
            generatedMapping.put(index, String.format("%%%s%%", value));
            index++;
        }
    }

    /**
     * Dynamically generates a query part for a boolean.
     *
     * @param value     The value to set.
     * @param attribute The attribute name.
     */
    private static void generateQueryPartBoolean(Boolean value, String attribute) {
        logger.debug("generateQueryPartBoolean '({}, {})'", value, attribute);

        if (value == false) {
            generatedQuery += String.format(" AND %s = ?", attribute);
            generatedMapping.put(index, value);
            index++;
        }
    }

    public static String getGeneratedQuery() {
        return generatedQuery;
    }

    public static Map<Integer, Object> getGeneratedMapping() {
        return generatedMapping;
    }
}
package sepm.ws16.e1226745.persistence;

import org.h2.tools.RunScript;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sepm.ws16.e1226745.exceptions.DaoException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by chop91 on 08.10.16.
 * <p>
 * Singleton handling the connection to the database.
 */
public class JdbcConnection {

    private static final Logger logger = LoggerFactory.getLogger(JdbcConnection.class);

    private static Connection connection;

    /**
     * Private constructor.
     */
    private JdbcConnection() throws ClassNotFoundException, SQLException, FileNotFoundException {
        logger.debug("JdbcConnection (private constructor)");

        Class.forName("org.h2.Driver");
        connection = DriverManager.getConnection("jdbc:h2:mem:horseSimulator", "sa", "");
        resetDatabase();
        connection.setAutoCommit(false);
    }

    /**
     * Returns an unique connection using the singleton pattern.
     *
     * @return The connection.
     * @throws DaoException If the class is not found, there are troubles with the database or the SQL script is not found.
     */
    public static Connection getConnection() throws DaoException {
        logger.debug("getConnection");

        if (connection == null) {
            try {
                new JdbcConnection();
                logger.info("Connected to the database.");
            } catch (ClassNotFoundException | SQLException | FileNotFoundException e) {
                logger.error(e.getMessage());
                throw new DaoException(e.getMessage());
            }
        }
        return connection;
    }

    /**
     * Closes the connection.
     *
     * @throws DaoException If there are troubles with the database.
     */
    public static void closeConnection() throws DaoException {
        logger.debug("closeConnection");

        if (connection != null) {
            try {
                connection.close();
                logger.info("Connection to the database closed.");
            } catch (SQLException e) {
                logger.error(e.getMessage());
                throw new DaoException(e.getMessage());
            }
        }
    }

    /**
     * Resets the database.
     *
     * @throws SQLException          If there are troubles with the database.
     * @throws FileNotFoundException If the drop or create script is not found.
     */
    private static void resetDatabase() throws SQLException, FileNotFoundException {
        logger.debug("resetDatabase");

        RunScript.execute(connection, new FileReader("./res/sql/drop.sql"));
        RunScript.execute(connection, new FileReader("./res/sql/create.sql"));
    }

    /**
     * Loads the test data.
     *
     * @throws DaoException If there are troubles with the database or the insert script is not found.
     */
    public static void loadTestData() throws DaoException {
        logger.debug("loadTestData");

        try {
            JdbcConnection.resetDatabase();
            RunScript.execute(JdbcConnection.getConnection(), new FileReader("./res/sql/insert.sql"));
        } catch (SQLException | FileNotFoundException e) {
            logger.error(e.getMessage());
            throw new DaoException(e.getMessage());
        }
    }
}
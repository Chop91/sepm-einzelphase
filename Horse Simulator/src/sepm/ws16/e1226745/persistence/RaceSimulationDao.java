package sepm.ws16.e1226745.persistence;

import sepm.ws16.e1226745.entities.persistence.RaceSimulationEntry;
import sepm.ws16.e1226745.entities.search.RaceSimulationSearch;
import sepm.ws16.e1226745.exceptions.DaoException;

import java.util.List;

/**
 * Created by chop91 on 09.10.16.
 */
public interface RaceSimulationDao {
    /**
     * Creates a new race simulation.
     *
     * @param raceSimulationEntries The race simulation to create.
     * @return The created race simulation.
     * @throws DaoException If raceSimulationEntries is null.
     */
    public List<RaceSimulationEntry> create(List<RaceSimulationEntry> raceSimulationEntries) throws DaoException;

    /**
     * Returns a list of race simulations that match specific search attributes for race simulations.
     *
     * @param raceSimulationSearch The search attributes.
     * @return The list of race simulations that match the attributes.
     * @throws DaoException If raceSimulationSearch is null.
     */
    public List<RaceSimulationEntry> read(RaceSimulationSearch raceSimulationSearch) throws DaoException;
}
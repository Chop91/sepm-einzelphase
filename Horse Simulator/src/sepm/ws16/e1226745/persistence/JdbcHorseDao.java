package sepm.ws16.e1226745.persistence;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sepm.ws16.e1226745.entities.persistence.Horse;
import sepm.ws16.e1226745.entities.search.HorseSearch;
import sepm.ws16.e1226745.exceptions.DaoException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by chop91 on 27.10.16.
 */
public class JdbcHorseDao implements HorseDao {

    private static final Logger logger = LoggerFactory.getLogger(JdbcHorseDao.class);

    private Connection connection;

    private static final String CREATE_STATEMENT = "INSERT INTO Horse(Name, Age, MinSpeed, MaxSpeed, ImagePath) VALUES (?, ?, ?, ?, ?);";
    private static final String UPDATE_STATEMENT = "UPDATE Horse SET Name = ?, Age = ?, MinSpeed = ?, MaxSpeed = ?, ImagePath = ?, IsDeleted = ? WHERE Id = ?;";

    public JdbcHorseDao(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Horse create(Horse horse) throws DaoException {
        logger.debug("create '{}'", horse);

        checkIfHorseIsNull(horse);

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(CREATE_STATEMENT, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, horse.getName());
            preparedStatement.setInt(2, horse.getAge());
            preparedStatement.setDouble(3, horse.getMinSpeed());
            preparedStatement.setDouble(4, horse.getMaxSpeed());
            preparedStatement.setString(5, horse.getImagePath());
            preparedStatement.executeUpdate();

            // Set the provided id in the entity and update it
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            resultSet.next();
            horse.setId(resultSet.getInt(1));
            horse.setDeleted(false);
            update(horse);
            logger.debug("created '{}'", horse);
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e.getMessage());
        }
        return horse;
    }

    @Override
    public List<Horse> read(HorseSearch horseSearch) throws DaoException {
        logger.debug("read '{}'", horseSearch);

        checkIfHorseSearchIsNull(horseSearch);

        // Create a dynamic query for the provided parameters
        DynamicQueryGenerator.generateQuery(horseSearch);
        String statement = DynamicQueryGenerator.getGeneratedQuery();
        Map<Integer, Object> map = DynamicQueryGenerator.getGeneratedMapping();

        ArrayList<Horse> horses = new ArrayList();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(statement);
            for (Map.Entry<Integer, Object> entry : map.entrySet()) {
                preparedStatement.setObject(entry.getKey(), entry.getValue());
            }

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                horses.add(new Horse(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3), resultSet.getDouble(4), resultSet.getDouble(5), resultSet.getString(6), resultSet.getBoolean(7)));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e.getMessage());
        }

        return horses;
    }

    @Override
    public List<Horse> readAll() throws DaoException {
        logger.debug("readAll");
        return read(new HorseSearch(null, true));
    }

    @Override
    public List<Horse> readAllUndeleted() throws DaoException {
        logger.debug("readAllUndeleted");
        return read(new HorseSearch(null, false));
    }

    @Override
    public void update(Horse horse) throws DaoException {
        logger.debug("update '{}'", horse);

        checkIfHorseExists(horse);

        try {
            // Update horse
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_STATEMENT);
            preparedStatement.setString(1, horse.getName());
            preparedStatement.setInt(2, horse.getAge());
            preparedStatement.setDouble(3, horse.getMinSpeed());
            preparedStatement.setDouble(4, horse.getMaxSpeed());
            preparedStatement.setString(5, horse.getImagePath());
            preparedStatement.setBoolean(6, horse.getDeleted());
            preparedStatement.setInt(7, horse.getId());
            preparedStatement.executeUpdate();

            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                logger.error(e.getMessage());
                throw new DaoException(e.getMessage());
            }
        }
    }

    @Override
    public void delete(Horse horse) throws DaoException {
        logger.debug("delete '{}'", horse);

        checkIfHorseExists(horse);
        horse.setDeleted(true);
        update(horse);
    }

    /**
     * Checks if the horse search is null.
     *
     * @param horseSearch The search parameter.
     * @throws DaoException If the horse search is null.
     */
    private void checkIfHorseSearchIsNull(HorseSearch horseSearch) throws DaoException {
        logger.debug("checkIfHorseSearchIsNull '{}'", horseSearch);

        if (horseSearch == null) {
            logAndThrowDaoException(String.format("'HorseSearch' is null."));
        }
    }

    /**
     * Checks if the horse is null.
     *
     * @param horse The horse.
     * @throws DaoException If the horse is null.
     */
    private void checkIfHorseIsNull(Horse horse) throws DaoException {
        logger.debug("checkIfHorseIsNull '{}'", horse);

        if (horse == null) {
            logAndThrowDaoException(String.format("'Horse' is null."));
        }
    }

    /**
     * Checks if the horse exists.
     *
     * @param horse The horse.
     * @throws DaoException If the horse does not exist.
     */
    private void checkIfHorseExists(Horse horse) throws DaoException {
        logger.debug("checkIfHorseExists '{}'", horse);

        checkIfHorseIsNull(horse);

        HorseSearch horseSearch = new HorseSearch(horse.getId(), false);
        List<Horse> horses = read(horseSearch);
        if (horses.isEmpty()) {
            logAndThrowDaoException(String.format("'Horse' %s does not exist.", horse));
        }
    }

    /**
     * Logs and throws a dao exception.
     *
     * @param message The message to log and throw.
     * @throws DaoException Always, because it is the purpose of this method.
     */
    private static void logAndThrowDaoException(String message) throws DaoException {
        logger.warn(message);
        throw new DaoException(message);
    }
}
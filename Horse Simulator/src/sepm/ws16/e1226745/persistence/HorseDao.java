package sepm.ws16.e1226745.persistence;

import sepm.ws16.e1226745.entities.persistence.Horse;
import sepm.ws16.e1226745.entities.search.HorseSearch;
import sepm.ws16.e1226745.exceptions.DaoException;

import java.util.List;

/**
 * Created by chop91 on 09.10.16.
 */
public interface HorseDao {
    /**
     * Creates a new horse.
     *
     * @param horse The horse to create.
     * @return The created horse.
     * @throws DaoException If horse is null, speed is invalid or a SQL exception occurred.
     */
    public Horse create(Horse horse) throws DaoException;

    /**
     * Returns a list of horses that match specific search attributes for horses.
     *
     * @param horseSearch The search attributes.
     * @return The list of horses that match the attributes.
     * @throws DaoException If horseSearch is null or a SQL exception occurred.
     */
    public List<Horse> read(HorseSearch horseSearch) throws DaoException;

    /**
     * Returns a list that contain all horses including already deleted one.
     *
     * @return The list of horses.
     * @throws DaoException If horseSearch is null or a SQL exception occurred.
     */
    public List<Horse> readAll() throws DaoException;

    /**
     * Returns a list that contain all horses without already deleted one.
     *
     * @return The list of horses.
     * @throws DaoException If Horse is null or a SQL exception occurred.
     */
    public List<Horse> readAllUndeleted() throws DaoException;

    /**
     * Updates a horse with specific values.
     *
     * @param horse The specific values of the horse to update.
     * @throws DaoException If horse is null, a horse with the given id does not exist or a SQL exception occurred.
     */
    public void update(Horse horse) throws DaoException;

    /**
     * Deletes a horse.
     *
     * @param horse The horse to delete.
     * @throws DaoException If horse is null.
     */
    public void delete(Horse horse) throws DaoException;
}
package sepm.ws16.e1226745.persistence;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sepm.ws16.e1226745.entities.persistence.RaceSimulationEntry;
import sepm.ws16.e1226745.entities.search.RaceSimulationSearch;
import sepm.ws16.e1226745.exceptions.DaoException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by chop91 on 29.10.16.
 */
public class JdbcRaceSimulationDao implements RaceSimulationDao {

    private static final Logger logger = LoggerFactory.getLogger(JdbcRaceSimulationDao.class);

    private Connection connection;

    private static final String CREATE_STATEMENT = "INSERT INTO RaceSimulation(Id, HorseId, HorseName, JockeyId, JockeyName, AverageSpeed, RandomSpeed, LuckFactor, CalculatedSkill, Rank) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
    private static final String SELECT_NEXTVAL = "SELECT RaceSimulationSequence.NEXTVAL FROM dual;";

    public JdbcRaceSimulationDao(Connection connection) {
        this.connection = connection;
    }

    @Override
    public List<RaceSimulationEntry> create(List<RaceSimulationEntry> raceSimulationEntries) throws DaoException {
        logger.debug("create '{}'", raceSimulationEntries);

        checkIfRaceSimulationEntriesIsNull(raceSimulationEntries);

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_NEXTVAL);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                Integer id = resultSet.getInt(1);
                for (RaceSimulationEntry raceSimulationEntry : raceSimulationEntries) {
                    raceSimulationEntry.setId(id);

                    preparedStatement = connection.prepareStatement(CREATE_STATEMENT);
                    preparedStatement.setInt(1, raceSimulationEntry.getId());
                    preparedStatement.setInt(2, raceSimulationEntry.getHorseId());
                    preparedStatement.setString(3, raceSimulationEntry.getHorseName());
                    preparedStatement.setInt(4, raceSimulationEntry.getJockeyId());
                    preparedStatement.setString(5, raceSimulationEntry.getJockeyName());
                    preparedStatement.setDouble(6, raceSimulationEntry.getAverageSpeed());
                    preparedStatement.setDouble(7, raceSimulationEntry.getRandomSpeed());
                    preparedStatement.setDouble(8, raceSimulationEntry.getLuckFactor());
                    preparedStatement.setDouble(9, raceSimulationEntry.getCalculatedSkill());
                    preparedStatement.setInt(10, raceSimulationEntry.getRank());
                    preparedStatement.executeUpdate();
                    logger.debug("created '{}'", raceSimulationEntries);
                }
            }
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                logger.error(e.getMessage());
                throw new DaoException(e.getMessage());
            }
        }
        return raceSimulationEntries;
    }

    @Override
    public List<RaceSimulationEntry> read(RaceSimulationSearch raceSimulationSearch) throws DaoException {
        logger.debug("read '{}'", raceSimulationSearch);

        checkIfRaceSimulationSearchIsNull(raceSimulationSearch);

        // Create a dynamic query for the provided parameters
        DynamicQueryGenerator.generateQuery(raceSimulationSearch);
        String statement = DynamicQueryGenerator.getGeneratedQuery();
        Map<Integer, Object> map = DynamicQueryGenerator.getGeneratedMapping();

        ArrayList<RaceSimulationEntry> raceSimulationEntries = new ArrayList();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(statement);
            for (Map.Entry<Integer, Object> entry : map.entrySet()) {
                preparedStatement.setObject(entry.getKey(), entry.getValue());
            }

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                raceSimulationEntries.add(new RaceSimulationEntry(resultSet.getInt(1), resultSet.getInt(2), resultSet.getString(3), resultSet.getInt(4), resultSet.getString(5), resultSet.getDouble(6), resultSet.getDouble(7), resultSet.getDouble(8), resultSet.getDouble(9), resultSet.getInt(10)));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e.getMessage());
        }

        return raceSimulationEntries;
    }


    /**
     * Checks if the race simulation search is null.
     *
     * @param raceSimulationSearch The search parameter.
     * @throws DaoException If the race simulation search is null.
     */
    private void checkIfRaceSimulationSearchIsNull(RaceSimulationSearch raceSimulationSearch) throws DaoException {
        logger.debug("checkIfRaceSimulationSearchIsNull '{}'", raceSimulationSearch);

        if (raceSimulationSearch == null) {
            logAndThrowDaoException(String.format("'RaceSimulationSearch' is null."));
        }
    }

    /**
     * Checks if the list of race simulation entris is null.
     *
     * @param raceSimulationEntries The list of race simulation entries.
     * @throws DaoException If the list of race simulation entries is null.
     */
    private void checkIfRaceSimulationEntriesIsNull(List<RaceSimulationEntry> raceSimulationEntries) throws DaoException {
        logger.debug("checkIfRaceSimulationEntriesIsNull '{}'", raceSimulationEntries);

        if (raceSimulationEntries == null) {
            logAndThrowDaoException(String.format("'List<RaceSimulationEntry>' is null."));
        }
    }

    /**
     * Logs and throws a dao exception.
     *
     * @param message The message to log and throw.
     * @throws DaoException Always, because it is the purpose of this method.
     */
    private static void logAndThrowDaoException(String message) throws DaoException {
        logger.warn(message);
        throw new DaoException(message);
    }
}
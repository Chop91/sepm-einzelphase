package sepm.ws16.e1226745.persistence;

import sepm.ws16.e1226745.entities.persistence.Jockey;
import sepm.ws16.e1226745.entities.search.JockeySearch;
import sepm.ws16.e1226745.exceptions.DaoException;

import java.util.List;

/**
 * Created by chop91 on 09.10.16.
 */
public interface JockeyDao {
    /**
     * Creates a new jockey.
     *
     * @param jockey The jockey to create.
     * @return The created jockey.
     * @throws DaoException If jockey is null, age is negative or a SQL exception occurred.
     */
    public Jockey create(Jockey jockey) throws DaoException;

    /**
     * Returns a list of jockeys that match specific search attributes for jockeys.
     *
     * @param jockeySearch The search attributes.
     * @return The list of jockeys that match the attributes.
     * @throws DaoException If jockeySearch is null or a SQL exception occurred.
     */
    public List<Jockey> read(JockeySearch jockeySearch) throws DaoException;

    /**
     * Returns a list that contain all jockeys including already deleted one.
     *
     * @return The list of jockeys.
     * @throws DaoException If jockeySearch is null or a SQL exception occurred.
     */
    public List<Jockey> readAll() throws DaoException;

    /**
     * Returns a list that contain all jockeys without already deleted one.
     *
     * @return The list of jockeys.
     * @throws DaoException If jockeySearch is null or a SQL exception occurred.
     */
    public List<Jockey> readAllUndeleted() throws DaoException;

    /**
     * Updates a jockey with specific values.
     *
     * @param jockey The specific values of the jockey to update.
     * @throws DaoException If jockey is null, a jockey with the given id does not exist or a SQL exception occurred.
     */
    public void update(Jockey jockey) throws DaoException;

    /**
     * Deletes a jockey.
     *
     * @param jockey The jockey to delete.
     * @throws DaoException If jockey is null.,
     */
    public void delete(Jockey jockey) throws DaoException;
}
package sepm.ws16.e1226745.service;

import sepm.ws16.e1226745.entities.persistence.Horse;
import sepm.ws16.e1226745.entities.persistence.Jockey;
import sepm.ws16.e1226745.entities.persistence.RaceSimulationEntry;
import sepm.ws16.e1226745.entities.search.HorseSearch;
import sepm.ws16.e1226745.entities.search.JockeySearch;
import sepm.ws16.e1226745.entities.search.RaceSimulationSearch;
import sepm.ws16.e1226745.exceptions.ServiceException;

import java.util.List;
import java.util.Map;

/**
 * Created by chop91 on 29.10.16.
 */
public interface Service {
    // Horse

    /**
     * Creates a new horse.
     *
     * @param horse The horse to create.
     * @return The created horse.
     * @throws ServiceException If there are troubles during the DAO access or there are invalid parameters.
     */
    public Horse createHorse(Horse horse) throws ServiceException;

    /**
     * Returns a list of horses that match specific search attributes for horses.
     *
     * @param horseSearch The search attributes.
     * @return The list of horses that match the attributes.
     * @throws ServiceException If there are troubles during the DAO access or there are invalid parameters.
     */
    public List<Horse> readHorses(HorseSearch horseSearch) throws ServiceException;

    /**
     * Returns a list that contain all horses including already deleted one.
     *
     * @return The list of horses.
     * @throws ServiceException If there are troubles during the DAO access.
     */
    public List<Horse> readAllHorses() throws ServiceException;

    /**
     * Returns a list that contain all horses without already deleted one.
     *
     * @return The list of horses.
     * @throws ServiceException If there are troubles during the DAO access.
     */
    public List<Horse> readAllUndeletedHorses() throws ServiceException;

    /**
     * Updates a horse with specific values.
     *
     * @param horse The specific values of the horse to update.
     * @throws ServiceException If there are troubles during the DAO access or there are invalid parameters.
     */
    public void updateHorse(Horse horse) throws ServiceException;

    /**
     * Deletes a horse.
     *
     * @param horse The horse to delete.
     * @throws ServiceException If there are troubles during the DAO access or there are invalid parameters.
     */
    public void deleteHorse(Horse horse) throws ServiceException;


    // Jockey

    /**
     * Creates a new jockey.
     *
     * @param jockey The jockey to create.
     * @return The created jockey.
     * @throws ServiceException If there are troubles during the DAO access or there are invalid parameters.
     */
    public Jockey createJockey(Jockey jockey) throws ServiceException;

    /**
     * Returns a list of jockeys that match specific search attributes for jockeys.
     *
     * @param jockeySearch The search attributes.
     * @return The list of jockeys that match the attributes.
     * @throws ServiceException If there are troubles during the DAO access or there are invalid parameters.
     */
    public List<Jockey> readJockeys(JockeySearch jockeySearch) throws ServiceException;

    /**
     * Returns a list that contain all jockeys including already deleted one.
     *
     * @return The list of jockeys.
     * @throws ServiceException If there are troubles during the DAO access.
     */
    public List<Jockey> readAllJockeys() throws ServiceException;

    /**
     * Returns a list that contain all jockeys without already deleted one.
     *
     * @return The list of jockeys.
     * @throws ServiceException If there are troubles during the DAO access.
     */
    public List<Jockey> readAllUndeletedJockeys() throws ServiceException;


    /**
     * Updates a jockey with specific values.
     *
     * @param jockey The specific values of the jockey to update.
     * @throws ServiceException If there are troubles during the DAO access or there are invalid parameters.
     */
    public void updateJockey(Jockey jockey) throws ServiceException;

    /**
     * Deletes a jockey.
     *
     * @param jockey The jockey to delete.
     * @throws ServiceException If there are troubles during the DAO access or there are invalid parameters.
     */
    public void deleteJockey(Jockey jockey) throws ServiceException;


    // Service Stuff

    /**
     * Add a horse-jockey combination as competitors.
     *
     * @param horse  The horse in the competition.
     * @param jockey The jockey in the competition.
     * @throws ServiceException If horse or jockey is already participating or one of them has invalid parameters.
     */
    public void addHorseJockeyPairToRaceSimulation(Horse horse, Jockey jockey) throws ServiceException;

    /**
     * Removes a horse-jockey combination from the competition.
     *
     * @param horse  The horse in the competition.
     * @param jockey The jockey in the competition.
     * @throws ServiceException If horse or jockey does not participate or one of them has invalid parameters.
     */
    public void removeHorseJockeyPairFromRaceSimulation(Horse horse, Jockey jockey) throws ServiceException;

    /**
     * Creates a new race simulation.
     */
    public void createRaceSimulation();

    /**
     * Executes and return the race simulation with the previously added horse-jockey combinations.
     *
     * @return The race simulation.
     * @throws ServiceException If there are no competitors or there are troubles during the DAO access.
     */
    public List<RaceSimulationEntry> executeRaceSimulation() throws ServiceException;

    /**
     * Searches for a specific race simulation with a combination of given ids from race, horse, jockey.
     *
     * @param raceSimulationSearch The search parameter.
     * @return The race simulations that match the conditions.
     * @throws ServiceException If there are troubles during the DAO access or there are invalid parameters.
     */
    public List<RaceSimulationEntry> searchRaceSimulation(RaceSimulationSearch raceSimulationSearch) throws ServiceException;

    /**
     * Generates the statistics for a specific horse, jockey or horse-jockey combination simulation.
     *
     * @param raceSimulationSearch The search parameters.
     * @return The statistics.
     * @throws ServiceException If there are troubles during the DAO access or there are invalid parameters.
     */
    public Map<Integer, Long> generateStatistics(RaceSimulationSearch raceSimulationSearch) throws ServiceException;

    /**
     * Generates some test data by using some provided SQL scripts.
     *
     * @throws ServiceException If there are troubles during the DAO access.
     */
    public void generateTestData() throws ServiceException;

    /**
     * Closes the service.
     *
     * @throws ServiceException If there are troubles during the DAO access.
     */
    public void closeService() throws ServiceException;

    /**
     * Gets all competitors of all race simulations.
     *
     * @return The competitors of all race simulations.
     */
    public Map<Horse, Jockey> getAllCompetitors();

    /**
     * Gets the current competitors of the last created race simulation.
     *
     * @return The current competitors of the last created race simulation.
     */
    public Map<Horse, Jockey> getCurrentCompetitors();
}
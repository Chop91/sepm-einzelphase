package sepm.ws16.e1226745.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sepm.ws16.e1226745.entities.persistence.Horse;
import sepm.ws16.e1226745.entities.persistence.Jockey;
import sepm.ws16.e1226745.entities.search.HorseSearch;
import sepm.ws16.e1226745.entities.search.JockeySearch;
import sepm.ws16.e1226745.entities.search.RaceSimulationSearch;
import sepm.ws16.e1226745.exceptions.ValidationException;

/**
 * Created by chop91 on 29.10.16.
 */
public class Validator {

    private static final Logger logger = LoggerFactory.getLogger(Validator.class);

    private static final int MAX_CHARS = 100;
    private static final int MIN_SPEED = 40;
    private static final int MAX_SPEED = 60;

    /**
     * Validates the parameter of a horse.
     *
     * @param horse The horse to validate.
     * @throws ValidationException If one or more parameters are invalid.
     */
    public static void validate(Horse horse) throws ValidationException {
        logger.debug("Validate '{}'", horse);

        if (horse == null) {
            logAndThrowValidationException(String.format("'Horse' is null."));
        }

        if (horse.getId() != null && horse.getId() < 1) {
            logAndThrowValidationException(String.format("'Id' is invalid).", horse.getId()));
        }

        if (horse.getName() == null || horse.getName().isEmpty()) {
            logAndThrowValidationException(String.format("'Name' is not specified."));
        } else if (horse.getName().length() > MAX_CHARS) {
            logAndThrowValidationException(String.format("'Name' is too long (%d/%d characters used).", horse.getName().length(), MAX_CHARS));
        }

        if (horse.getAge() == null) {
            logAndThrowValidationException(String.format("'Age' is not specified."));
        } else if (horse.getAge() < 0) {
            logAndThrowValidationException(String.format("'Age' is negative."));
        }

        if (horse.getMinSpeed() == null) {
            logAndThrowValidationException(String.format("'Min Speed' is not specified."));
        } else if (horse.getMinSpeed() < MIN_SPEED || horse.getMinSpeed() > MAX_SPEED) {
            logAndThrowValidationException(String.format("'Min Speed' is out of range [%d, %d].", MIN_SPEED, MAX_SPEED));
        }

        if (horse.getMaxSpeed() == null) {
            logAndThrowValidationException(String.format("'Max Speed' is not specified."));
        } else if (horse.getMaxSpeed() < MIN_SPEED || horse.getMaxSpeed() > MAX_SPEED) {
            logAndThrowValidationException(String.format("'Max Speed' is out of range [%d, %d].", MIN_SPEED, MAX_SPEED));
        }

        if (horse.getMaxSpeed() < horse.getMinSpeed()) {
            logAndThrowValidationException(String.format("'Max Speed' is smaller than 'Min Speed' (%f < %f).", horse.getMaxSpeed(), horse.getMinSpeed()));
        }

        if (horse.getImagePath() == null || horse.getImagePath().isEmpty()) {
            logAndThrowValidationException(String.format("'Image Path' is not specified."));
        } else if (horse.getImagePath().length() > MAX_CHARS) {
            logAndThrowValidationException(String.format("'Image Path' is too long (%d/%d characters used).", horse.getImagePath().length(), MAX_CHARS));
        }
    }

    /**
     * Validates the parameter of a horse search.
     *
     * @param horseSearch The horseSearch to validate.
     * @throws ValidationException If one or more parameters are invalid.
     */
    public static void validate(HorseSearch horseSearch) throws ValidationException {
        logger.debug("Validate '{}'", horseSearch);

        if (horseSearch == null) {
            logAndThrowValidationException(String.format("'HorseSearch' is null."));
        }

        if (horseSearch.getId() != null && horseSearch.getId() < 1) {
            logAndThrowValidationException(String.format("'Id' is invalid).", horseSearch.getId()));
        }

        if (horseSearch.getName() != null && horseSearch.getName().length() > MAX_CHARS) {
            logAndThrowValidationException(String.format("'Name' is too long (%d/%d characters used).", horseSearch.getName().length(), MAX_CHARS));
        }

        if (horseSearch.getLowerAge() != null && horseSearch.getLowerAge() < 0) {
            logAndThrowValidationException(String.format("'Age (min)' is negative."));
        }

        if (horseSearch.getUpperAge() != null && horseSearch.getUpperAge() < 0) {
            logAndThrowValidationException(String.format("'Age (max)' is negative."));
        }

        if (horseSearch.getUpperAge() != null && horseSearch.getLowerAge() != null && horseSearch.getUpperAge() < horseSearch.getLowerAge()) {
            logAndThrowValidationException(String.format("'Age (max)' is smaller than 'Age (min)' (%d < %d).", horseSearch.getUpperAge(), horseSearch.getLowerAge()));
        }

        if (horseSearch.getLowerMinSpeed() != null && (horseSearch.getLowerMinSpeed() < MIN_SPEED || horseSearch.getLowerMinSpeed() > MAX_SPEED)) {
            logAndThrowValidationException(String.format("'Min Speed (min)' is out of range [%d, %d].", MIN_SPEED, MAX_SPEED));
        }

        if (horseSearch.getUpperMinSpeed() != null && (horseSearch.getUpperMinSpeed() < MIN_SPEED || horseSearch.getUpperMinSpeed() > MAX_SPEED)) {
            logAndThrowValidationException(String.format("'Min Speed (max)' is out of range [%d, %d].", MIN_SPEED, MAX_SPEED));
        }

        if (horseSearch.getLowerMaxSpeed() != null && (horseSearch.getLowerMaxSpeed() < MIN_SPEED || horseSearch.getLowerMaxSpeed() > MAX_SPEED)) {
            logAndThrowValidationException(String.format("'Max Speed (min)' is out of range [%d, %d].", MIN_SPEED, MAX_SPEED));
        }

        if (horseSearch.getUpperMaxSpeed() != null && (horseSearch.getUpperMaxSpeed() < MIN_SPEED || horseSearch.getUpperMaxSpeed() > MAX_SPEED)) {
            logAndThrowValidationException(String.format("'Max Speed (max)' is out of range [%d, %d].", MIN_SPEED, MAX_SPEED));
        }

        if (horseSearch.getUpperMinSpeed() != null && horseSearch.getLowerMinSpeed() != null && horseSearch.getUpperMinSpeed() < horseSearch.getLowerMinSpeed()) {
            logAndThrowValidationException(String.format("'Min Speed (max)' is smaller than 'Min Speed (min)' (%f < %f).", horseSearch.getUpperMinSpeed(), horseSearch.getLowerMinSpeed()));
        }

        if (horseSearch.getUpperMaxSpeed() != null && horseSearch.getLowerMaxSpeed() != null && horseSearch.getUpperMaxSpeed() < horseSearch.getLowerMaxSpeed()) {
            logAndThrowValidationException(String.format("'Max Speed (max)' is smaller than 'Max Speed (min)' (%f < %f).", horseSearch.getUpperMaxSpeed(), horseSearch.getLowerMaxSpeed()));
        }
    }

    /**
     * Validates the parameter of a jockey.
     *
     * @param jockey The jockey to validate.
     * @throws ValidationException If one or more parameters are invalid.
     */
    public static void validate(Jockey jockey) throws ValidationException {
        logger.debug("Validate '{}'", jockey);

        if (jockey == null) {
            logAndThrowValidationException(String.format("'Jockey' is null."));
        }

        if (jockey.getId() != null && jockey.getId() < 1) {
            logAndThrowValidationException(String.format("'Id' is invalid).", jockey.getId()));
        }

        if (jockey.getFirstName() == null || jockey.getFirstName().isEmpty()) {
            logAndThrowValidationException(String.format("'First Name' is not specified."));
        } else if (jockey.getFirstName().length() > MAX_CHARS) {
            logAndThrowValidationException(String.format("'First Name' is too long (%d/%d characters used).", jockey.getFirstName().length(), MAX_CHARS));
        }

        if (jockey.getLastName() == null || jockey.getLastName().isEmpty()) {
            logAndThrowValidationException(String.format("'Last Name' is not specified."));
        } else if (jockey.getLastName().length() > MAX_CHARS) {
            logAndThrowValidationException(String.format("'Last Name' is too long (%d/%d characters used).", jockey.getLastName().length(), MAX_CHARS));
        }

        if (jockey.getAge() == null) {
            logAndThrowValidationException(String.format("'Age' is not specified."));
        } else if (jockey.getAge() < 0) {
            logAndThrowValidationException(String.format("'Age' is negative."));
        }

        if (jockey.getSkill() == null) {
            logAndThrowValidationException(String.format("'Skill' is not specified."));
        }

        if (jockey.getImagePath() == null || jockey.getImagePath().isEmpty()) {
            logAndThrowValidationException(String.format("'Image Path' is not specified."));
        } else if (jockey.getImagePath().length() > MAX_CHARS) {
            logAndThrowValidationException(String.format("'Image Path' is too long (%d/%d characters used).", jockey.getImagePath().length(), MAX_CHARS));
        }
    }

    /**
     * Validates the parameter of a jockey search.
     *
     * @param jockeySearch The jockeySearch to validate.
     * @throws ValidationException If one or more parameters are invalid.
     */
    public static void validate(JockeySearch jockeySearch) throws ValidationException {
        logger.debug("Validate '{}'", jockeySearch);

        if (jockeySearch == null) {
            logAndThrowValidationException(String.format("'JockeySearch' is null."));
        }

        if (jockeySearch.getId() != null && jockeySearch.getId() < 1) {
            logAndThrowValidationException(String.format("'Id' is invalid).", jockeySearch.getId()));
        }

        if (jockeySearch.getFirstName() != null && jockeySearch.getFirstName().length() > MAX_CHARS) {
            logAndThrowValidationException(String.format("'First Name' is too long (%d/%d characters used).", jockeySearch.getFirstName().length(), MAX_CHARS));
        }

        if (jockeySearch.getLastName() != null && jockeySearch.getLastName().length() > MAX_CHARS) {
            logAndThrowValidationException(String.format("'Last Name' is too long (%d/%d characters used).", jockeySearch.getFirstName().length(), MAX_CHARS));
        }

        if (jockeySearch.getLowerAge() != null && jockeySearch.getLowerAge() < 0) {
            logAndThrowValidationException(String.format("'Age (min)' is negative."));
        }

        if (jockeySearch.getUpperAge() != null && jockeySearch.getUpperAge() < 0) {
            logAndThrowValidationException(String.format("'Age (max)' is negative."));
        }

        if (jockeySearch.getUpperAge() != null && jockeySearch.getLowerAge() != null && jockeySearch.getUpperAge() < jockeySearch.getLowerAge()) {
            logAndThrowValidationException(String.format("'Age (max)' is smaller than 'Age (min)' (%d < %d).", jockeySearch.getUpperAge(), jockeySearch.getLowerAge()));
        }

        if (jockeySearch.getUpperSkill() != null && jockeySearch.getLowerSkill() != null && jockeySearch.getUpperSkill() < jockeySearch.getLowerSkill()) {
            logAndThrowValidationException(String.format("'Skill (max)' is smaller than 'Skill (min)' (%f < %f).", jockeySearch.getUpperSkill(), jockeySearch.getLowerSkill()));
        }
    }

    /**
     * Validates the parameter of a race simulation search.
     *
     * @param raceSimulationSearch The raceSimulationSearch to validate.
     * @throws ValidationException If one or more parameters are invalid.
     */
    public static void validate(RaceSimulationSearch raceSimulationSearch) throws ValidationException {
        logger.debug("Validate '{}'", raceSimulationSearch);

        if (raceSimulationSearch == null) {
            logAndThrowValidationException(String.format("'RaceSimulationSearch' is null."));
        }

        if (raceSimulationSearch.getId() != null && raceSimulationSearch.getId() < 1) {
            logAndThrowValidationException(String.format("'Id' %d is invalid.", raceSimulationSearch.getId()));
        }

        if (raceSimulationSearch.getHorseId() != null && raceSimulationSearch.getHorseId() < 1) {
            logAndThrowValidationException(String.format("'HorseId' %d is invalid.", raceSimulationSearch.getId()));
        }

        if (raceSimulationSearch.getJockeyId() != null && raceSimulationSearch.getJockeyId() < 1) {
            logAndThrowValidationException(String.format("'JockeyId' %d is invalid.", raceSimulationSearch.getId()));
        }
    }

    /**
     * Logs and throws a validation exception.
     *
     * @param message The message to log and throw.
     * @throws ValidationException Always, because it is the purpose of this method.
     */
    private static void logAndThrowValidationException(String message) throws ValidationException {
        logger.warn(message);
        throw new ValidationException(message);
    }
}
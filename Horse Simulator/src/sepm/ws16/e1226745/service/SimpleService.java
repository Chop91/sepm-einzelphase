package sepm.ws16.e1226745.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sepm.ws16.e1226745.entities.persistence.Horse;
import sepm.ws16.e1226745.entities.persistence.Jockey;
import sepm.ws16.e1226745.entities.persistence.RaceSimulationEntry;
import sepm.ws16.e1226745.entities.search.HorseSearch;
import sepm.ws16.e1226745.entities.search.JockeySearch;
import sepm.ws16.e1226745.entities.search.RaceSimulationSearch;
import sepm.ws16.e1226745.exceptions.DaoException;
import sepm.ws16.e1226745.exceptions.ServiceException;
import sepm.ws16.e1226745.exceptions.ValidationException;
import sepm.ws16.e1226745.persistence.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by chop91 on 29.10.16.
 */
public class SimpleService implements Service {

    private static final Logger logger = LoggerFactory.getLogger(SimpleService.class);

    private HorseDao horseDao;
    private JockeyDao jockeyDao;
    private RaceSimulationDao raceSimulationDao;
    private Map<Horse, Jockey> allCompetitors;
    private Map<Horse, Jockey> currentCompetitors;

    private static final Random RANDOM = new Random();
    private static final Double LUCK_FACTOR_MIN = 0.95;
    private static final Double LUCK_FACTOR_MAX = 1.05;

    public SimpleService() throws ServiceException {
        try {
            this.horseDao = new JdbcHorseDao(JdbcConnection.getConnection());
            this.jockeyDao = new JdbcJockeyDao(JdbcConnection.getConnection());
            this.raceSimulationDao = new JdbcRaceSimulationDao(JdbcConnection.getConnection());
            this.allCompetitors = new HashMap<>();
            this.currentCompetitors = new HashMap<>();
            logger.info("Service started.");
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public Horse createHorse(Horse horse) throws ServiceException {
        logger.debug("Enter createHorse.");
        try {
            Validator.validate(horse);
            return horseDao.create(horse);
        } catch (DaoException | ValidationException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public List<Horse> readHorses(HorseSearch horseSearch) throws ServiceException {
        try {
            logger.debug("Enter readHorses.");
            Validator.validate(horseSearch);
            return horseDao.read(horseSearch);
        } catch (DaoException | ValidationException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public List<Horse> readAllHorses() throws ServiceException {
        logger.debug("Enter readAllHorses.");
        try {
            return horseDao.readAll();
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public List<Horse> readAllUndeletedHorses() throws ServiceException {
        logger.debug("Enter readAllUndeletedHorses.");
        try {
            return horseDao.readAllUndeleted();
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void updateHorse(Horse horse) throws ServiceException {
        logger.debug("Enter updateHorse.");
        try {
            Validator.validate(horse);
            horseDao.update(horse);
        } catch (DaoException | ValidationException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void deleteHorse(Horse horse) throws ServiceException {
        logger.debug("Enter deleteHorse.");
        try {
            Validator.validate(horse);
            horseDao.delete(horse);
        } catch (DaoException | ValidationException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public Jockey createJockey(Jockey jockey) throws ServiceException {
        logger.debug("Enter createJockey.");
        try {
            Validator.validate(jockey);
            return jockeyDao.create(jockey);
        } catch (DaoException | ValidationException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public List<Jockey> readJockeys(JockeySearch jockeySearch) throws ServiceException {
        logger.debug("Enter readJockeys.");
        try {
            Validator.validate(jockeySearch);
            return jockeyDao.read(jockeySearch);
        } catch (DaoException | ValidationException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public List<Jockey> readAllJockeys() throws ServiceException {
        logger.debug("Enter readAllJockeys.");
        try {
            return jockeyDao.readAll();
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public List<Jockey> readAllUndeletedJockeys() throws ServiceException {
        logger.debug("Enter readAllUndeletedJockeys.");
        try {
            return jockeyDao.readAllUndeleted();
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void updateJockey(Jockey jockey) throws ServiceException {
        logger.debug("Enter updateJockey.");
        try {
            Validator.validate(jockey);
            jockeyDao.update(jockey);
        } catch (DaoException | ValidationException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void deleteJockey(Jockey jockey) throws ServiceException {
        logger.debug("Enter deleteJockey.");
        try {
            Validator.validate(jockey);
            jockeyDao.delete(jockey);
        } catch (DaoException | ValidationException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void addHorseJockeyPairToRaceSimulation(Horse horse, Jockey jockey) throws ServiceException {
        logger.debug("Enter addHorseJockeyPairToRaceSimulation.");
        try {
            Validator.validate(horse);
            if (currentCompetitors.containsKey(horse)) {
                logAndThrowServiceException(String.format("Horse (ID = %d) is already part of the race simulation.", horse.getId()));
            }

            Validator.validate(jockey);
            if (currentCompetitors.containsValue(jockey)) {
                logAndThrowServiceException(String.format("Jockey (ID = %d) is already part of the race simulation.", jockey.getId()));
            }

            currentCompetitors.put(horse, jockey);
            logger.debug("Added '{}' and '{}'.", horse, jockey);
        } catch (ValidationException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void removeHorseJockeyPairFromRaceSimulation(Horse horse, Jockey jockey) throws ServiceException {
        logger.debug("Enter removeHorseJockeyPairFromRaceSimulation.");
        try {
            Validator.validate(horse);
            if (!currentCompetitors.containsKey(horse)) {
                logAndThrowServiceException(String.format("Horse (ID = %d) does not exist in the race simulation.", horse.getId()));
            }

            Validator.validate(jockey);
            if (!currentCompetitors.containsValue(jockey)) {
                logAndThrowServiceException(String.format("Jockey (ID = %d) does not exist in the race simulation.", jockey.getId()));
            }

            boolean isRemoved = currentCompetitors.remove(horse, jockey);
            if (!isRemoved) {
                logAndThrowServiceException(String.format("Horse(ID = %d)-Jockey(ID = %d) combination does not exist in the race simulation.", horse.getId(), jockey.getId()));
            }
            logger.debug("Removed '{}' and '{}'.", horse, jockey);
        } catch (ValidationException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void createRaceSimulation() {
        logger.debug("Enter createRaceSimulation.");
        currentCompetitors.clear();
    }

    @Override
    public List<RaceSimulationEntry> executeRaceSimulation() throws ServiceException {
        logger.debug("Enter executeRaceSimulation.");
        if (currentCompetitors.isEmpty()) {
            logAndThrowServiceException(String.format("No Horse-Jockey combination exists in the race simulation."));
        }

        TreeSet<RaceSimulationEntry> raceSimulationEntries = new TreeSet<>();
        for (Map.Entry<Horse, Jockey> entry : currentCompetitors.entrySet()) {
            Horse horse = entry.getKey();
            Jockey jockey = entry.getValue();

            Double randomSpeed = generateRandomNumberBetweenMinAndMax(horse.getMinSpeed(), horse.getMaxSpeed());
            Double luckFactor = generateRandomNumberBetweenMinAndMax(LUCK_FACTOR_MIN, LUCK_FACTOR_MAX);
            Double calculatedSkill = 1.0 + (0.15 * (1.0 / Math.PI) * Math.atan((1.0 / 5.0) * jockey.getSkill()));
            Double averageSpeed = randomSpeed * calculatedSkill * luckFactor;

            RaceSimulationEntry raceSimulationEntry = new RaceSimulationEntry(null, horse.getId(), horse.getName(), jockey.getId(), String.format("%s %s", jockey.getFirstName(), jockey.getLastName()), averageSpeed, randomSpeed, luckFactor, calculatedSkill, null);
            raceSimulationEntries.add(raceSimulationEntry);
            logger.debug("Added '{}'.", raceSimulationEntry);
        }

        Iterator<RaceSimulationEntry> iterator = raceSimulationEntries.iterator();
        int i = 1;
        while (iterator.hasNext()) {
            RaceSimulationEntry raceSimulationEntry = iterator.next();
            raceSimulationEntry.setRank(i);
            i++;
            logger.debug("Set rank '{}'.", raceSimulationEntry);
        }

        logger.debug("Start race simulation.");
        ArrayList<RaceSimulationEntry> raceSimulationEntriesList = new ArrayList<>(raceSimulationEntries);
        try {
            raceSimulationDao.create(raceSimulationEntriesList);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }
        logger.debug("Race simulation successful.");

        return raceSimulationEntriesList;
    }

    @Override
    public List<RaceSimulationEntry> searchRaceSimulation(RaceSimulationSearch raceSimulationSearch) throws ServiceException {
        logger.debug("Enter searchRaceSimulation.");
        try {
            Validator.validate(raceSimulationSearch);
            return raceSimulationDao.read(raceSimulationSearch);
        } catch (DaoException | ValidationException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public Map<Integer, Long> generateStatistics(RaceSimulationSearch raceSimulationSearch) throws ServiceException {
        logger.debug("Enter generateStatistics.");
        List<RaceSimulationEntry> raceSimulationEntries = searchRaceSimulation(raceSimulationSearch);
        Map<Integer, Long> occurrences = raceSimulationEntries.stream().collect(Collectors.groupingBy(e -> e.getRank(), Collectors.counting()));
        return occurrences;
    }

    @Override
    public void generateTestData() throws ServiceException {
        logger.debug("Enter generateTestData.");
        try {
            JdbcConnection.loadTestData();
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void closeService() throws ServiceException {
        logger.debug("Enter closeService.");
        try {
            JdbcConnection.closeConnection();
            logger.info("Service closed.");
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Generates a random number between a min and max value.
     *
     * @param min The min value.
     * @param max The max value.
     * @return The generated random number.
     */
    private Double generateRandomNumberBetweenMinAndMax(Double min, Double max) {
        logger.debug("Enter generateRandomNumberBetweenMinAndMax.");
        return min + (max - min) * RANDOM.nextDouble();
    }

    /**
     * Logs and throws a service exception.
     *
     * @param message The message to log and throw.
     * @throws ServiceException Always, because it is the purpose of this method.
     */
    private static void logAndThrowServiceException(String message) throws ServiceException {
        logger.warn(message);
        throw new ServiceException(message);
    }

    public Map<Horse, Jockey> getAllCompetitors() {
        return allCompetitors;
    }

    public Map<Horse, Jockey> getCurrentCompetitors() {
        return currentCompetitors;
    }
}
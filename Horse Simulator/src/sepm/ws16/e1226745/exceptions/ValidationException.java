package sepm.ws16.e1226745.exceptions;

/**
 * Created by chop91 on 29.10.16.
 */
public class ValidationException extends Exception {
    public ValidationException() {
        super();
    }

    public ValidationException(String message) {
        super(message);
    }
}
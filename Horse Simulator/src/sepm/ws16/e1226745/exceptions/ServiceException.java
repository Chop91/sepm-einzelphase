package sepm.ws16.e1226745.exceptions;

/**
 * Created by chop91 on 29.10.16.
 */
public class ServiceException extends Exception {
    public ServiceException() {
        super();
    }

    public ServiceException(String message) {
        super(message);
    }
}
package sepm.ws16.e1226745.exceptions;

/**
 * Created by chop91 on 08.10.16.
 */
public class DaoException extends Exception {
    public DaoException() {
        super();
    }

    public DaoException(String message) {
        super(message);
    }
}